# ProfinetExplorer
## Installation
To install download the [installer](https://gitlab.com/profinetexplorer/profinetexplorer/raw/master/bootstrapper/bin/Release/ProfinetExplorer.exe) and execute it.
During the installation you will be asked to install SharpPcap if it is not found on your system. SharpPcap is a program
to monitor network traffic and is needed to communicate with the ProfinetExplorer devices.

## Original Creator
This project is a fork of [ProfinetExplorer on sourceforge.net](https://sourceforge.net/projects/profinetexplorer/).
It was created by Morten Kvistgaard.

## Features
- Profinet DCP (Device Config, Get, Set, Discover)
- Profinet IO (Connect, Write, Control, Release)
- GSDML (Not much yet)
- Profinet Cyclic/Acylic data
- Demo server (DCP)

## Maintainer
This project was developed as a student project at the Cooperative State University (DHBW) in Stuttgart by the following student team in 2018/2019:
 - Marius Hauser
 - Lorenz Krause
 - Michael Schweiker
 - Nicole Wagner
 - Bastian Krug
 - Max Dettmann
 - Ali Dinc

Supervisor: Markus Rentschler (http://wwwlehre.dhbw-stuttgart.de/~rentschler/) and Christian Ewertz

Special thanks to Steffen Gerdes for the help with C# ;)