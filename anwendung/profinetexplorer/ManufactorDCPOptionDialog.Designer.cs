﻿namespace ProfinetExplorer
{
    partial class ManufactorDcpOptionDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_OKButton = new System.Windows.Forms.Button();
            this.m_CancelButton = new System.Windows.Forms.Button();
            this.NameText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.OptionValue = new System.Windows.Forms.NumericUpDown();
            this.SubOptionValue = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.IsReadableCheck = new System.Windows.Forms.CheckBox();
            this.IsWriteableCheck = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.OptionValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubOptionValue)).BeginInit();
            this.SuspendLayout();
            // 
            // m_OKButton
            // 
            this.m_OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.m_OKButton.Location = new System.Drawing.Point(135, 143);
            this.m_OKButton.Name = "m_OKButton";
            this.m_OKButton.Size = new System.Drawing.Size(75, 23);
            this.m_OKButton.TabIndex = 0;
            this.m_OKButton.Text = "OK";
            this.m_OKButton.UseVisualStyleBackColor = true;
            // 
            // m_CancelButton
            // 
            this.m_CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.m_CancelButton.Location = new System.Drawing.Point(54, 143);
            this.m_CancelButton.Name = "m_CancelButton";
            this.m_CancelButton.Size = new System.Drawing.Size(75, 23);
            this.m_CancelButton.TabIndex = 1;
            this.m_CancelButton.Text = "Cancel";
            this.m_CancelButton.UseVisualStyleBackColor = true;
            // 
            // NameText
            // 
            this.NameText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NameText.Location = new System.Drawing.Point(75, 12);
            this.NameText.Name = "NameText";
            this.NameText.Size = new System.Drawing.Size(132, 20);
            this.NameText.TabIndex = 2;
            this.NameText.Text = "ManufactorOptionName";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Name";
            // 
            // OptionValue
            // 
            this.OptionValue.Location = new System.Drawing.Point(75, 38);
            this.OptionValue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.OptionValue.Name = "OptionValue";
            this.OptionValue.Size = new System.Drawing.Size(58, 20);
            this.OptionValue.TabIndex = 4;
            // 
            // SubOptionValue
            // 
            this.SubOptionValue.Location = new System.Drawing.Point(75, 64);
            this.SubOptionValue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.SubOptionValue.Name = "SubOptionValue";
            this.SubOptionValue.Size = new System.Drawing.Size(58, 20);
            this.SubOptionValue.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Option";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "SubOption";
            // 
            // IsReadableCheck
            // 
            this.IsReadableCheck.AutoSize = true;
            this.IsReadableCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.IsReadableCheck.Location = new System.Drawing.Point(50, 90);
            this.IsReadableCheck.Name = "IsReadableCheck";
            this.IsReadableCheck.Size = new System.Drawing.Size(83, 17);
            this.IsReadableCheck.TabIndex = 8;
            this.IsReadableCheck.Text = "Is Readable";
            this.IsReadableCheck.UseVisualStyleBackColor = true;
            // 
            // IsWriteableCheck
            // 
            this.IsWriteableCheck.AutoSize = true;
            this.IsWriteableCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.IsWriteableCheck.Location = new System.Drawing.Point(51, 113);
            this.IsWriteableCheck.Name = "IsWriteableCheck";
            this.IsWriteableCheck.Size = new System.Drawing.Size(82, 17);
            this.IsWriteableCheck.TabIndex = 9;
            this.IsWriteableCheck.Text = "Is Writeable";
            this.IsWriteableCheck.UseVisualStyleBackColor = true;
            // 
            // ManufactorDCPOptionDialog
            // 
            this.AcceptButton = this.m_OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.m_CancelButton;
            this.ClientSize = new System.Drawing.Size(222, 178);
            this.Controls.Add(this.IsWriteableCheck);
            this.Controls.Add(this.IsReadableCheck);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.SubOptionValue);
            this.Controls.Add(this.OptionValue);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NameText);
            this.Controls.Add(this.m_CancelButton);
            this.Controls.Add(this.m_OKButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "ManufactorDcpOptionDialog";
            this.Text = "Add Manufactor DCP Entry";
            ((System.ComponentModel.ISupportInitialize)(this.OptionValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubOptionValue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_OKButton;
        private System.Windows.Forms.Button m_CancelButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox NameText;
        public System.Windows.Forms.NumericUpDown OptionValue;
        public System.Windows.Forms.NumericUpDown SubOptionValue;
        public System.Windows.Forms.CheckBox IsReadableCheck;
        public System.Windows.Forms.CheckBox IsWriteableCheck;
    }
}