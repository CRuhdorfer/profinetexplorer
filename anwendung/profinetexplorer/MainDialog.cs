﻿/**************************************************************************
*                           MIT License
* 
* Copyright (C) 2014 Morten Kvistgaard <mk@pch-engineering.dk>
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*********************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Profinet;
using ProfinetExplorer.MainComponent;
using ProfinetExplorer.MainComponent.Menu;


//includes for GSD-File Parsing
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Gsd2Aml.Lib;
using Gsd2Aml.Lib.Models;
using Gsd2Aml.Lib.Logging;


namespace ProfinetExplorer
{
    public partial class MainDialog : Form
    {
        private Networks _networks;
        private ToolStripButton _mDataGridWriteButton;
        private ToolStripButton _mDataGridReadButton;
        private ToolStripButton _mDataGridInsertGenericButton;
        //private Dictionary<Dcp.DeviceIdInfo, System.Xml.XmlDocument> _mGsdmlFiles = null;
        public Dictionary<Dcp.DeviceIdInfo, ISO15745Profile> _mParsedGsdmlFiles = null;


        public ISharpPcapFactory sharpPcapFactory;

        public MainDialog(): this(new SharpPcapFactory())
        {
        }

        public MainDialog(ISharpPcapFactory sharp)
        {
            InitializeComponent();
            Trace.Listeners.Add(new Utilities.MyTraceListener(this));
            m_DeviceTree.ExpandAll();
            Utilities.EventLogging.initLogFile(DateTime.Now);
            Gsd2Aml.Lib.Util.Logger = new Utilities.EventLogging(this);

            //load splitter setup
            try
            {
                if (Properties.Settings.Default.GUI_FormSize != new Size(0, 0))
                    this.Size = Properties.Settings.Default.GUI_FormSize;
                FormWindowState state = (FormWindowState)Enum.Parse(typeof(FormWindowState), Properties.Settings.Default.GUI_FormState);
                if (state != FormWindowState.Minimized)
                    this.WindowState = state;
                if (Properties.Settings.Default.GUI_SplitterButtom != -1)
                    m_SplitContainerButtom.SplitterDistance = Properties.Settings.Default.GUI_SplitterButtom;
                if (Properties.Settings.Default.GUI_SplitterLeft != -1)
                    m_SplitContainerLeft.SplitterDistance = Properties.Settings.Default.GUI_SplitterLeft;
                if (Properties.Settings.Default.GUI_SplitterRight != -1)
                    m_SplitContainerRight.SplitterDistance = Properties.Settings.Default.GUI_SplitterRight;
            }
            catch
            {
                //ignore
            }
            _networks = new Networks(m_DeviceTree);
            sharpPcapFactory = sharp;
        }

        
        
        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingsDialog dlg = new SettingsDialog();
            dlg.SelectedObject = Properties.Settings.Default;
            dlg.ShowDialog(this);
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            //close all transports
            foreach (Network n in _networks.Values)
            {
                n.EthernetTransport.Close();
            }

            try
            {
                //commit setup
                Properties.Settings.Default.GUI_SplitterButtom = m_SplitContainerButtom.SplitterDistance;
                Properties.Settings.Default.GUI_SplitterLeft = m_SplitContainerLeft.SplitterDistance;
                Properties.Settings.Default.GUI_SplitterRight = m_SplitContainerRight.SplitterDistance;
                Properties.Settings.Default.GUI_FormSize = this.Size;
                Properties.Settings.Default.GUI_FormState = this.WindowState.ToString();

                //save
                Properties.Settings.Default.Save();
            }
            catch (Exception)
            {
            }
        }

        #region Network related Calls

        public void AddNetworkInterface()
        {
            ComboDialog dlg = new ComboDialog();
            this.Cursor = Cursors.WaitCursor;
            dlg.Text = "Add network";
            dlg.LabelText = "Select network to add";
            bool addNetworkSuccessfull = false;
            try
            {
                foreach (SharpPcap.ICaptureDevice dev in sharpPcapFactory.GetInstance())
                {
                    dlg.ComboBox.Items.Add(dev.Description);
                }
                if (dlg.ComboBox.Items.Count > 0) dlg.ComboBox.SelectedIndex = 0;
                dlg.Width = 430;
                if (dlg.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                {
                    // zeile 329 oder so 
                    // AddNetwork bekommt nen Netzwerk Interface uebergeben
                    _networks.AddNetwork(SharpPcap.CaptureDeviceList.Instance[dlg.ComboBox.SelectedIndex], true);
                    addNetworkSuccessfull = true; 
                }
            }
            catch (Exception ex)
            {
                ShowWarningBox(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

            if (addNetworkSuccessfull)
            {
                /*  
                 * this method tries to connect with the choosen network
                 * if the connection was successfull, a broadcast is send
                 */
                try
                {
                    SearchSelectedNetwork();
                }
                catch(Exception ex)
                {
                    ShowWarningBox(ex);
                }
            }
        }

        public void ShowWarningBox(Exception ex)
        {
            Utilities.EventLogging.logEvent(this, GetWarningText(ex), true, "Warning");
        }

        public string GetWarningText(Exception ex)
        {
            if (ex.GetType() == typeof(DllNotFoundException))
            {
                return "No WinPCAP found, install WinPCAP first";
            }
            else if (ex.GetType() == typeof(SharpPcap.PcapException))
            {
                return "No interface found, please connect a wired interface. It is not possible to use a wireless connection due to the use of SharpPcap."; //TODO: reference in mod003 documentation
            }
            else if (ex.GetType() == typeof(ArgumentOutOfRangeException))
            {
                return "You have to select an Interface to add.";
            }
            else
            {
                return "An Exception occured, for further informations search for \"" + ex.Message + "\"";
            }
        }
        
        private void addNetworkInterfaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNetworkInterface();
        }

        private void m_AddInterfaceButton_Click(object sender, EventArgs e)
        {
            AddNetworkInterface();
        }

        private void RemoveSelectedNetwork()
        {
            string key = m_DeviceTree.SelectedNode.Name;
            _networks.RemoveNetwork(key);
        }

        private void m_RemoveNetworkInterfaceButton_Click(object sender, EventArgs e)
        {
            RemoveSelectedNetwork();
        }

        private void removeNetworkInterfaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoveSelectedNetwork();
        }

        #endregion
        //validate pcap dll
        public void check_pcap()
        {
            string pcapVersion = ProfinetEthernetTransport.PcapVersion;
            Utilities.EventLogging.logEvent(this, "Pcap version: " + pcapVersion, false, "information");

            //check for correct string
            if (string.IsNullOrEmpty(pcapVersion) || pcapVersion.ToLower().Contains("pcap version can't be identified"))
            {
                PCAPErrorDialog PCAPErrorDialog = new PCAPErrorDialog();
                PCAPErrorDialog.Show();
            }
        }
        private void m_delayedStart_Tick(object sender, EventArgs e)
        {
            m_delayedStart.Enabled = false;
            check_pcap(); 

            //load networks
            if (Properties.Settings.Default.Networks != null)
            {
                foreach(string name in Properties.Settings.Default.Networks)
                {
                    SharpPcap.ICaptureDevice network;
                    try
                    {
                        network = SharpPcap.CaptureDeviceList.Instance[name];
                    }
                    catch (Exception)
                    {
                        network = null;
                    }
                    if(network != null) _networks.AddNetwork(network, false);
                }
            }

            //load gsdml files
            LoadAllGsdmlFiles();
        }

        private Network OpenEthernetNetwork()
        {
            //get mac
            if (m_DeviceTree.SelectedNode == null || m_DeviceTree.SelectedNode.Level < 1)
            {
                Utilities.EventLogging.logEvent(this, "No network selected", true, "warning");
                return null;
            }
            string key;
            if (m_DeviceTree.SelectedNode.Level == 1)
            {
                key = m_DeviceTree.SelectedNode.Name;
            }
            else
            {
                key = m_DeviceTree.SelectedNode.Parent.Name;
            }

            Network network = _networks.GetNetwork(key);

            if (!network.EthernetTransport.IsOpen)
            {
                network.EthernetTransport.Open();
                network.EthernetTransport.OnDcpMessage += new ProfinetEthernetTransport.OnDcpMessageHandler(EthernetTransport_OnDcpMessage);
                network.EthernetTransport.OnAcyclicMessage += new ProfinetEthernetTransport.OnAcyclicMessageHandler(EthernetTransport_OnAcyclicMessage);
                network.EthernetTransport.OnCyclicMessage += new ProfinetEthernetTransport.OnCyclicMessageHandler(EthernetTransport_OnCyclicMessage);
            }
            return network;
        }

        private Network OpenUdpNetwork(string interfaceKey, string deviceMac)
        {
            Network network = _networks.GetNetwork(interfaceKey);
            if (network.UdpTransport == null)
            {
                //get matching ip
                Dcp.IpInfo deviceIp = (Dcp.IpInfo)network.Devices[deviceMac][Dcp.BlockOptions.IpIpParameter];
                string interfaceIp = ProfinetEthernetTransport.GetLocalIpAddress(deviceIp.Ip.ToString());

                //open
                network.UdpTransport = new ProfinetUdpTransport(interfaceIp, network.EthernetTransport.Adapter.MacAddress);
                network.UdpTransport.OnRpcMessageReceived += new ProfinetUdpTransport.OnRpcMessageReceivedHandler(UdpTransport_OnRPCMessageReceived);
                try
                {
                    network.UdpTransport.InputCycleTimeMs = (ushort)Properties.Settings.Default.DefaultCycleTimeMs;
                    network.UdpTransport.OutputCycleTimeMs = (ushort)Properties.Settings.Default.DefaultCycleTimeMs;
                }
                catch
                {
                    string errorMessage = "Cycle time not supported\nDefaulted to " + network.UdpTransport.InputCycleTimeMs + " ms";
                    Utilities.EventLogging.logEvent(this, errorMessage, true, "Cycle time");
                }
            }
            return network;
        }

        private void UdpTransport_OnRPCMessageReceived(ConnectionInfoUdp sender, Rpc.PacketTypes type, Rpc.Flags1 flags1, Rpc.Flags2 flags2, Rpc.Encodings encoding, ushort serialHighLow, Guid objectId, Guid interfaceId, Guid activityId, uint serverBootTime, uint sequenceNo, Rpc.Operations op, ushort bodyLength, ushort fragmentNo, System.IO.Stream data)
        {
            if (type == Rpc.PacketTypes.Request)
            {
                //the device wants something
                if (op == Rpc.Operations.Control)
                {
                    UInt32 argsMaximumOrPnioStatus; UInt32 argsLength; UInt32 maximumCount; UInt32 offset; UInt32 actualCount;    //bogus data mostly
                    Rpc.DecodeNdrDataHeader(data, encoding, out argsMaximumOrPnioStatus, out argsLength, out maximumCount, out offset, out actualCount);
                    ProfinetIo.ControlBlockConnect ctrl = new ProfinetIo.ControlBlockConnect();
                    ctrl.Deserialize(data);
                    sender.Adapter.ControlResponse(activityId, sequenceNo, ProfinetIo.BlockTypes.IoxBlockResControlBlockConnectApplicationReady, ProfinetIo.ControlCommands.Done);
                }
            }
        }

        private static string DisplayStream(System.IO.Stream stream, int length)
        {
            StringBuilder str = new StringBuilder();
            int l = Math.Min((int)(stream.Length - stream.Position), length);
            for (int i = 0; i < l; i++)
            {
                byte b = (byte)stream.ReadByte();
                str.Append(b.ToString("X2"));
            }
            return str.ToString();
        }

        private void EthernetTransport_OnCyclicMessage(ConnectionInfoEthernet sender, ushort cycleCounter, Rt.DataStatus dataStatus, Rt.TransferStatus transferStatus, System.IO.Stream data, int dataLength)
        {
            this.Invoke((MethodInvoker)delegate
            {
                //add if needed
                if (m_CyclicDataList.Items.Count == 0)
                {
                    ListViewItem itm = m_CyclicDataList.Items.Add("");
                    itm.SubItems.Add("");
                    itm.SubItems.Add("");
                }
                m_CyclicDataList.Items[0].Text = cycleCounter.ToString();
                m_CyclicDataList.Items[0].SubItems[1].Text = dataStatus.ToString();
                m_CyclicDataList.Items[0].SubItems[2].Text = DisplayStream(data, dataLength);
            });
        }

        private void EthernetTransport_OnAcyclicMessage(ConnectionInfoEthernet sender, ushort alarmDestinationEndpoint, ushort alarmSourceEndpoint, Rt.PduTypes pduType, Rt.AddFlags addFlags, ushort sendSeqNum, ushort ackSeqNum, ushort varPartLen, System.IO.Stream data)
        {
            string text = "";
            if ((pduType & Rt.PduTypes.Err) == Rt.PduTypes.Err)
            {
                ProfinetIo.PnioStatus pnioStatus = new ProfinetIo.PnioStatus();
                pnioStatus.Deserialize(data);
                text = pnioStatus.ToString();
            }

            this.Invoke((MethodInvoker)delegate
            {
                ListViewItem itm = m_AcyclicDataList.Items.Add(alarmDestinationEndpoint.ToString());
                itm.SubItems.Add(alarmSourceEndpoint.ToString());
                itm.SubItems.Add(((Rt.PduTypes)((byte)pduType & 0xF)).ToString());
                itm.SubItems.Add(text);
            });
        }

        private void EthernetTransport_OnDcpMessage(ConnectionInfoEthernet sender, Dcp.ServiceIds serviceId, uint xid, ushort responseDelayFactor, Dictionary<Dcp.BlockOptions, object> blocks)
        {
            if (serviceId == Profinet.Dcp.ServiceIds.IdentifyResponse)
            {
                string mac = sender.Source.ToString();
                Network net = _networks.GetNetwork(sender.Adapter.Adapter.Name);
                if (net.Devices.ContainsKey(mac)) net.Devices.Remove(mac);
                net.Devices.Add(mac, blocks);
                this.Invoke((MethodInvoker)delegate
                {
                    TreeNode parent = m_DeviceTree.Nodes[0].Nodes[sender.Adapter.Adapter.Name];
                    if (parent.Nodes.ContainsKey(mac)) parent.Nodes.RemoveByKey(mac);
                    TreeNode node = parent.Nodes.Add(mac, (string)blocks[Profinet.Dcp.BlockOptions.DevicePropertiesNameOfStation], 2);
                    node.SelectedImageIndex = node.ImageIndex;
                    m_DeviceTree.ExpandAll();
                    Utilities.EventLogging.logEvent(null, "Added device " + node.Text, false, "information");
                });
            }
        }

        private void SearchSelectedNetwork()
        {
            //get network
            Network conn = OpenEthernetNetwork();
            if (conn == null) return;

            //Send Search
            conn.EthernetTransport.SendIdentifyBroadcast();
        }

        private void m_SearchToolButton_Click(object sender, EventArgs e)
        {
            SearchSelectedNetwork();
        }

        private void searchForDevicesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchSelectedNetwork();
        }

        private void LoadAllGsdmlFiles()
        {
            if(_mParsedGsdmlFiles != null) return; //already loaded

            if (Properties.Settings.Default.GDSMLFilePaths == null) Properties.Settings.Default.GDSMLFilePaths = new System.Collections.Generic.List<string>();
            _mParsedGsdmlFiles = new Dictionary<Dcp.DeviceIdInfo, ISO15745Profile>();
            foreach (var str in Properties.Settings.Default.GDSMLFilePaths)
            {
                try
                {
                    LoadGsdmlFile(str);

                    ////add to settings
                    //Properties.Settings.Default.GDSMLFilePaths.Add(str);
                    //Trace.TraceInformation("Loaded GDSML file " + System.IO.Path.GetFileNameWithoutExtension(str));
                }
                catch (Exception ex)
                {
                    Utilities.EventLogging.logEvent(null, "Couldn't load GDSML file \"" + str + "\"\n\nError: " + ex.Message, true, "warning");
                }
            }
            gsdml_MenuItem(); //add already loaded GSDML-files as DropDownMenuItems for editing
        }

        public int LoadGsdmlFile(string path)
        {
            //load all files if needed
            if (_mParsedGsdmlFiles == null) {  LoadAllGsdmlFiles(); }
           
            //search out device/vendor id
            try
            {
                ISO15745Profile parsedGsd = null;
                parsedGsd = parse_gsd(path); //parse xml to object 
                // System.Xml.XmlNode node = xml_gsd["ISO15745Profile"]["ProfileBody"]["DeviceIdentity"];
                //var test = 
                if (parsedGsd == null)
                {return 1; }
                Dcp.DeviceIdInfo id = new Dcp.DeviceIdInfo(
                    Convert.ToUInt16(parsedGsd.ProfileBody.DeviceIdentity.VendorID.ToString(), 16),
                    Convert.ToUInt16(parsedGsd.ProfileBody.DeviceIdentity.DeviceID.ToString(), 16));
                /*  adding parsed gsd file to gsd map   */
                
                if (_mParsedGsdmlFiles.ContainsKey(id))
                {
                    Utilities.EventLogging.logEvent(null, "Removing excess gsdml file", false, "information");
                    _mParsedGsdmlFiles.Remove(id);
                    Properties.Settings.Default.GDSMLFilePaths.Remove(path);
                }

                _mParsedGsdmlFiles.Add(id, parsedGsd);

                //Utilities.EventLogging.logFailedEvent(null, "Removing excess gsdml file", false, "information");
                string trace = "Loaded GSDML file " + System.IO.Path.GetFileNameWithoutExtension(path);
                Utilities.EventLogging.logEvent(null, trace, false, "information");

                return 0;
            }
            catch (Exception e)
            {
                if (!(e is NullReferenceException))
                {
                    var message = "Couldn't load device/vendor id in file. File cannot be reloaded.\n Please review your changes.";
                    Utilities.EventLogging.logEvent(null, message , true, "warning");
                    //throw new Exception("Couldn't load device/vendor id in file\n " + e.ToString());
                }

                return 1;
            }
        }


        private static TreeNode AddTreeNode(TreeNodeCollection parent, string name, int imageIndex)
        {
            var node = parent.Add(name);
            node.ImageIndex = imageIndex;
            node.SelectedImageIndex = node.ImageIndex;
            return node;
        }

        public void LoadDeviceGsdmlIntoTree()
        {
            //System.Xml.XmlNode tmpXml1;

            //get network
            Network conn = OpenEthernetNetwork();
            if (conn == null) return;

            //get device
            string mac = m_DeviceTree.SelectedNode.Name;
            System.Net.NetworkInformation.PhysicalAddress macAddress = System.Net.NetworkInformation.PhysicalAddress.Parse(m_DeviceTree.SelectedNode.Name);

            //get cached identify data
            Dictionary<Dcp.BlockOptions, object> device = conn.Devices[mac];

            //device id
            Dcp.DeviceIdInfo id = (Dcp.DeviceIdInfo)conn.Devices[mac][Dcp.BlockOptions.DevicePropertiesDeviceId];

            //find id in list
            if (_mParsedGsdmlFiles.ContainsKey(id))
            {
                try
                {
                //Text translation table
                    _mParsedGsdmlFiles.TryGetValue(id, out var gsdFile);
                    var filepath = gsdFile?.FilePath;

                    var translations =
                        gsdFile?.ProfileBody.ApplicationProcess.ExternalTextList.PrimaryLanguage.ToDictionary(
                            text => text.TextId, text => text.Value);



                    //Access Points
                    var parentTreeNode = AddTreeNode(m_AddressSpaceTree.Nodes, "Device Access Points", 4);
                    foreach (var DeviceAccessPointItem in gsdFile?.ProfileBody.ApplicationProcess.DeviceAccessPointList)
                    {
                            var subParentTreeNode = AddTreeNode(parentTreeNode.Nodes, translations[DeviceAccessPointItem.ModuleInfo.Name.TextId], 4);
                            foreach (var subsubNode in DeviceAccessPointItem.SubslotList)
                            {
                                AddTreeNode(subParentTreeNode.Nodes, translations[subsubNode.TextId], 4);
                            }
                    }
                    //Modules
                    var modules = AddTreeNode(m_AddressSpaceTree.Nodes, "Modules", 4);
                    foreach (var moduleItem in gsdFile?.ProfileBody.ApplicationProcess.ModuleList)
                    {
                        var subParentTreeNode = AddTreeNode(modules.Nodes,
                            translations[moduleItem.ModuleInfo.Name.TextId], 4);

                        foreach (var subsubNode in moduleItem.VirtualSubmoduleList)
                        {
                            var subsubParentTreeNode = AddTreeNode(subParentTreeNode.Nodes,
                                translations[subsubNode.ModuleInfo.Name.TextId], 4);
                            TreeNode ioNode;
                            var ioParentTreeNode = AddTreeNode(subsubParentTreeNode.Nodes, "Input", 4);
                            if (subsubNode.IOData.Input != null)
                            {
                                foreach (var subIoNode in subsubNode.IOData.Input.DataItem)
                                {
                                    ioNode = AddTreeNode(ioParentTreeNode.Nodes, translations[subIoNode.TextId], 4);
                                }
                            }

                            if (subsubNode.IOData.Output != null)
                            {
                                foreach (var subIoNode in subsubNode.IOData.Output.DataItem)
                                {
                                    ioNode = AddTreeNode(ioParentTreeNode.Nodes, translations[subIoNode.TextId], 4);

                                }
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    string trace = "Error while reading GSDML: " + ex.Message;
                    Utilities.EventLogging.logEvent(null, trace, false, "error");
                }
            }
            else
            {
                //display empty node
                AddTreeNode(m_AddressSpaceTree.Nodes, "Load GSDML ...", 16);
                string trace = "No GSDML file was found";
                Utilities.EventLogging.logEvent(null, trace, false, "information");
            }
        }

        private void m_DeviceTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode node, parent;

            if (e.Node.Level >= 2)   //is device node
            {
                //clear
                m_AddressSpaceTree.Nodes.Clear();

                //add identify node
                parent = AddTreeNode(m_AddressSpaceTree.Nodes, "Device Config (DCP)", 17);

                //add all DCP options
                foreach (string opt in Enum.GetNames(typeof(Dcp.BlockOptions)))
                {
                    node = AddTreeNode(parent.Nodes, opt, 9);
                    node.Tag = new Dcp.BlockOptionMeta((Dcp.BlockOptions)Enum.Parse(typeof(Dcp.BlockOptions), opt));
                }

                //also add any Manufactor options
                foreach (Dcp.BlockOptionMeta option in SerializedSettings.GetAppManufacturerSpecificDcpOptionList())
                {
                    node = AddTreeNode(parent.Nodes, option.Name, 9);
                    node.Tag = option;
                }

                //add gsdml
                LoadDeviceGsdmlIntoTree();

                //clear property list
                m_DataGrid.SelectedObject = null;
            }
        }

        private void UpdateGrid(Dictionary<Dcp.BlockOptions, object> blocks)
        {
            this.Cursor = Cursors.WaitCursor;
            if (blocks == null) blocks = new Dictionary<Dcp.BlockOptions, object>();
            try
            {
                //update grid
                Utilities.DynamicPropertyGridContainer bag = new Utilities.DynamicPropertyGridContainer();
                foreach (KeyValuePair<Dcp.BlockOptions, object> entry in blocks)
                {
                    bag.Add(new Utilities.CustomProperty(entry.Key.ToString(), entry.Value, entry.Value != null ? entry.Value.GetType() : typeof(string), false, "", "", null, new Dcp.BlockOptionMeta(entry.Key)));
                }
                m_DataGrid.SelectedObject = bag;
                m_DataGrid.ExpandAllGridItems();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private Dictionary<Dcp.BlockOptions, object> GenerateInputBlocks(Dcp.BlockOptions option)
        {
            Dictionary<Dcp.BlockOptions, object> ret = new Dictionary<Dcp.BlockOptions, object>();

            switch (option)
            {
                case Dcp.BlockOptions.ControlSignal:
                    ret.Add(option, (ushort)0x100);
                    break;
                default:
                    //add a genric byte list
                    ret.Add(option, new List<byte>());
                    break;
            }

            return ret;
        }

        private void m_AddressSpaceTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //get network
            Network conn = OpenEthernetNetwork();
            if (conn == null) return;

            //get device
            string mac = m_DeviceTree.SelectedNode.Name;
            System.Net.NetworkInformation.PhysicalAddress macAddress = System.Net.NetworkInformation.PhysicalAddress.Parse(m_DeviceTree.SelectedNode.Name);

            if (e.Node.Index == 0 && e.Node.Level == 0)   //identify
            {
                //get cached identify data
                Dictionary<Dcp.BlockOptions, object> device = conn.Devices[mac];

                //display
                UpdateGrid(device);
            }
            else if (e.Node.Tag is Dcp.BlockOptionMeta)
            {
                Dcp.BlockOptionMeta dcpAction = (Dcp.BlockOptionMeta)e.Node.Tag;
                Dictionary<Dcp.BlockOptions, object> blocks = null;

                if (dcpAction.IsReadable)
                {
                    //fetch data from device
                    blocks = conn.EthernetTransport.SendGetRequest(macAddress, Properties.Settings.Default.DefaultTimeoutMs, Properties.Settings.Default.DefaultRetries, dcpAction.BlockOption);
                }
                else if (dcpAction.IsWriteable)
                {
                    //generate input data
                    blocks = GenerateInputBlocks(dcpAction.BlockOption);
                }

                //display
                UpdateGrid(blocks);
            }
        }

        private void lEDFlashToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //get network
            Network conn = OpenEthernetNetwork();
            if (conn == null) return;

            //get device
            if (m_DeviceTree.SelectedNode.Level < 2)
            {
                Utilities.EventLogging.logEvent(null, "No device selected", false, "warning");
                return;
            }
            string mac = m_DeviceTree.SelectedNode.Name;
            System.Net.NetworkInformation.PhysicalAddress macAddress = System.Net.NetworkInformation.PhysicalAddress.Parse(m_DeviceTree.SelectedNode.Name);


            //Send
            this.Cursor = Cursors.WaitCursor;
            try
            {
                Dcp.BlockErrors err = conn.EthernetTransport.SendSetSignalRequest(macAddress, Properties.Settings.Default.DefaultTimeoutMs, Properties.Settings.Default.DefaultRetries);
                if (err != Dcp.BlockErrors.NoError)
                {
                    string errorMessage = "Device refuse: " + err.ToString();
                    Utilities.EventLogging.logEvent(this, errorMessage, true, "Device Error");
                }
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null) return null;

            if (obj is string)
                return Encoding.ASCII.GetBytes((string)obj);
            else if (obj is byte[])
                return (byte[])obj;
            else if (obj is ushort)
            {
                byte[] ret = BitConverter.GetBytes((ushort)obj);
                if (BitConverter.IsLittleEndian) Array.Reverse(ret);
                return ret;
            }
            else if (obj is byte)
                return new byte[] { (byte)obj };
            else if (obj is List<byte>)
                return ((List<byte>)obj).ToArray();
            else if (obj.GetType().IsEnum)
            {
                object converted = Convert.ChangeType(obj, Enum.GetUnderlyingType(obj.GetType()));
                return ObjectToByteArray(converted);
            }
            else if (obj is IProfinetSerialize)
            {
                System.IO.MemoryStream mem = new System.IO.MemoryStream();
                IProfinetSerialize serializeableObj = (IProfinetSerialize)obj;
                serializeableObj.Serialize(mem);
                return mem.ToArray();
            }
            else
                throw new NotImplementedException();
        }

        private void m_DataGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                //get network
                Network conn = OpenEthernetNetwork();
                if (conn == null) return;

                //get device
                if (m_DeviceTree.SelectedNode.Level < 2)
                {
                    Utilities.EventLogging.logEvent(null, "No device selected", false, "warning");
                    return;
                }
                string mac = m_DeviceTree.SelectedNode.Name;
                System.Net.NetworkInformation.PhysicalAddress macAddress = System.Net.NetworkInformation.PhysicalAddress.Parse(m_DeviceTree.SelectedNode.Name);

                
                Utilities.CustomPropertyDescriptor c = null;
                GridItem gridItem = e.ChangedItem;
                // Go up to the Property (could be a sub-element)

                do
                {
                    if (gridItem.PropertyDescriptor is Utilities.CustomPropertyDescriptor)
                        c = (Utilities.CustomPropertyDescriptor)gridItem.PropertyDescriptor;
                    else
                        gridItem = gridItem.Parent;

                } while ((c == null) && (gridItem != null));

                if (c == null) return; // never occur normaly

                //fetch property
                Dcp.BlockOptionMeta property = (Dcp.BlockOptionMeta)c.CustomProperty.Tag;
                //new value
                object newValue = gridItem.Value;

                //convert
                byte[] data = null;
                try
                {
                    data = ObjectToByteArray(newValue);
                }
                catch (Exception ex)
                {
                    string errorMessage = "Couldn't convert property: " + ex.Message;
                    Utilities.EventLogging.logEvent(this, errorMessage, true, "Error");
                    return;
                }

                //write
                try
                {
                    if(conn.EthernetTransport.SendSetRequest(macAddress, Properties.Settings.Default.DefaultTimeoutMs, Properties.Settings.Default.DefaultRetries, property.BlockOption, data) != Dcp.BlockErrors.NoError)
                    {
                        string errorMessage = "Couldn't write property: ";
                        Utilities.EventLogging.logEvent(this, errorMessage, true, "Communication Error");
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = "Error during write: " + ex.Message;
                    Utilities.EventLogging.logEvent(this, errorMessage, true, "Error");
                }

                //reload if needed
                //m_AddressSpaceTree_AfterSelect(this, new TreeViewEventArgs(m_AddressSpaceTree.SelectedNode));
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void addManufactorDCPEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ManufactorDcpOptionDialog dlg = new ManufactorDcpOptionDialog();
            if (dlg.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                //extract
                Dcp.BlockOptionMeta manufactorOption = new Dcp.BlockOptionMeta(dlg.NameText.Text, (byte)dlg.OptionValue.Value, (byte)dlg.SubOptionValue.Value, dlg.IsReadableCheck.Checked, dlg.IsWriteableCheck.Checked);

                //save 
                SerializedSettings.AddToAppManufacturerSpecificDcpOptionList(manufactorOption);

                //refresh list
                m_DeviceTree_AfterSelect(this, new TreeViewEventArgs(m_DeviceTree.SelectedNode));
            }
        }

        private void m_LEDFlashButton_Click(object sender, EventArgs e)
        {
            lEDFlashToolStripMenuItem_Click(this, null);
        }

        private void MainDialog_Load(object sender, EventArgs e)
        {
            //add button to propertygrid
            foreach (Control control in m_DataGrid.Controls)
            {
                if (control is ToolStrip)
                {
                    _mDataGridWriteButton = new ToolStripButton();
                    _mDataGridWriteButton.Click += new EventHandler(m_DataGridWriteButton_Click);
                    _mDataGridWriteButton.ToolTipText = "Rewrite all entries";
                    _mDataGridWriteButton.Image = m_ImageList.Images[18];
                    ((ToolStrip)control).Items.Add(_mDataGridWriteButton);

                    _mDataGridReadButton = new ToolStripButton();
                    _mDataGridReadButton.Click += new EventHandler(m_DataGridReadButton_Click);
                    _mDataGridReadButton.ToolTipText = "Reload all entries";
                    _mDataGridReadButton.Image = m_ImageList.Images[19];
                    ((ToolStrip)control).Items.Add(_mDataGridReadButton);

                    _mDataGridInsertGenericButton = new ToolStripButton();
                    _mDataGridInsertGenericButton.Click += new EventHandler(m_DataGridInsertGenericButton_Click);
                    _mDataGridInsertGenericButton.ToolTipText = "Insert empty/generic data";
                    _mDataGridInsertGenericButton.Image = m_ImageList.Images[20];
                    ((ToolStrip)control).Items.Add(_mDataGridInsertGenericButton);
                }
            }
        }

        private void m_DataGridInsertGenericButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_AddressSpaceTree.SelectedNode.Tag is Dcp.BlockOptionMeta)
                {
                    Dcp.BlockOptionMeta property = (Dcp.BlockOptionMeta) m_AddressSpaceTree.SelectedNode.Tag;
                    Dictionary<Dcp.BlockOptions, object> blocks = new Dictionary<Dcp.BlockOptions, object>();
                    blocks.Add(property.BlockOption, new List<byte>());
                    UpdateGrid(blocks);
                }
                else
                {
                    MessageBox.Show(this, "The 'Insert empty/generic data' only works with DCP nodes",
                        "Select DCP node", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            catch (Exception ex)
            {
                Utilities.EventLogging.logEvent(null, "Couldn't write option : " + ex.Message, false, "error");
            }
        }

        private void m_DataGridWriteButton_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                //get network
                Network conn = OpenEthernetNetwork();
                if (conn == null) return;

                //get device
                if (m_DeviceTree.SelectedNode.Level < 2)
                {
                    Utilities.EventLogging.logEvent(this, "No device selected", false, null);
                    return;
                }
                string mac = m_DeviceTree.SelectedNode.Name;
                System.Net.NetworkInformation.PhysicalAddress macAddress = System.Net.NetworkInformation.PhysicalAddress.Parse(m_DeviceTree.SelectedNode.Name);


                Utilities.DynamicPropertyGridContainer bag = (Utilities.DynamicPropertyGridContainer)m_DataGrid.SelectedObject;
                foreach (Utilities.CustomProperty p in bag)
                {
                    Dcp.BlockOptionMeta dcpAction = (Dcp.BlockOptionMeta)p.Tag;

                    //convert
                    byte[] data = ObjectToByteArray(p.Value);

                    //write 
                    try
                    {
                         if(conn.EthernetTransport.SendSetRequest(macAddress, Properties.Settings.Default.DefaultTimeoutMs, Properties.Settings.Default.DefaultRetries, dcpAction.BlockOption, data) != Dcp.BlockErrors.NoError)
                             Utilities.EventLogging.logEvent(null, "Couldn't write option " + dcpAction.Name, false, "warning");
                    }
                    catch (Exception ex)
                    {
                        Utilities.EventLogging.logEvent(null, "Couldn't write option " + dcpAction.Name + ": " + ex.Message, false, "error");
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.EventLogging.logEvent(null, "Couldn't write option : " + ex.Message, false, "error");
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void m_DataGridReadButton_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                //get network
                Network conn = OpenEthernetNetwork();
                if (conn == null) return;

                //get device
                if (m_DeviceTree.SelectedNode.Level < 2)
                {
                    Utilities.EventLogging.logEvent(null, "No device selected", false, "warning");
                    return;
                }
                string mac = m_DeviceTree.SelectedNode.Name;
                System.Net.NetworkInformation.PhysicalAddress macAddress = System.Net.NetworkInformation.PhysicalAddress.Parse(m_DeviceTree.SelectedNode.Name);

                if (m_AddressSpaceTree.SelectedNode.Tag is Dcp.BlockOptionMeta)
                {
                    //reload
                    m_AddressSpaceTree_AfterSelect(this, new TreeViewEventArgs(m_AddressSpaceTree.SelectedNode));
                }
                else
                {
                    //read all in current list
                    Utilities.DynamicPropertyGridContainer bag = (Utilities.DynamicPropertyGridContainer)m_DataGrid.SelectedObject;
                    foreach (Utilities.CustomProperty p in bag)
                    {
                        Dcp.BlockOptionMeta dcpAction = (Dcp.BlockOptionMeta)p.Tag;

                        //read 
                        try
                        {
                            Dictionary<Dcp.BlockOptions, object> blocks = conn.EthernetTransport.SendGetRequest(macAddress, Properties.Settings.Default.DefaultTimeoutMs, Properties.Settings.Default.DefaultRetries, dcpAction.BlockOption);
                            p.Value = blocks[dcpAction.BlockOption];
                        }
                        catch (Exception ex)
                        {
                            Utilities.EventLogging.logEvent(null, "Couldn't read option " + dcpAction.Name + ": " + ex.Message, false, "warning");
                        }
                    }
                }
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void m_ConnectIOButton_Click(object sender, EventArgs e)
        {
            connectIOToolStripMenuItem_Click(this, null);
        }

        private void m_DisconnectIOButton_Click(object sender, EventArgs e)
        {
            disconnectIOToolStripMenuItem_Click(this, null);
        }

        private void connectIOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //warn about connect sequence
            if(!Properties.Settings.Default.HasWarnedAboutConnectSequence)
            {
                string message = "It turns out, that the connect (setup) sequence requires intimate knowledge about the device. This information can (in theory) be extracted from the GSD file. This step is not implemented yet. So unless you manually adjust the code, it is unlikely to work.";
                Utilities.EventLogging.logEvent(this, message, true, "Connect incomplete");
                Properties.Settings.Default.HasWarnedAboutConnectSequence = true;
            }

            //get device
            if (m_DeviceTree.SelectedNode == null || m_DeviceTree.SelectedNode.Level != 2)
            {
                Utilities.EventLogging.logEvent(null, "No device selected", false, "warning");
                return;
            }
            string deviceMac = m_DeviceTree.SelectedNode.Name;
            string interfaceKey = m_DeviceTree.SelectedNode.Parent.Name;

            //get network
            Network conn = OpenUdpNetwork(interfaceKey, deviceMac);
            if (conn == null) return;
            System.Net.NetworkInformation.PhysicalAddress macAddress = System.Net.NetworkInformation.PhysicalAddress.Parse(deviceMac);

            //get device ip
            System.Net.IPAddress deviceIp = ((Dcp.IpInfo)conn.Devices[deviceMac][Dcp.BlockOptions.IpIpParameter]).Ip;
            Dcp.DeviceIdInfo deviceId = (Dcp.DeviceIdInfo)conn.Devices[deviceMac][Dcp.BlockOptions.DevicePropertiesDeviceId];

            //validate
            if (conn.UdpTransport.HasConnected)
            {
                MessageBox.Show(this, "Already connected", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //open udp driver
            try
            {
                conn.UdpTransport.Open(deviceIp.ToString(), deviceId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Couldn't open udp: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //send connect message
            try
            {
                conn.UdpTransport.ConnectRequest(Properties.Settings.Default.DefaultTimeoutMs, Properties.Settings.Default.DefaultRetries);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Couldn't connect to device: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //report 'parameter setup' done
            try
            {
                conn.UdpTransport.ControlRequest(ProfinetUdpTransport.Controls.ParameterEnd, Properties.Settings.Default.DefaultTimeoutMs, Properties.Settings.Default.DefaultRetries);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Couldn't control device: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void disconnectIOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //get device
            if (m_DeviceTree.SelectedNode == null || m_DeviceTree.SelectedNode.Level != 2)
            {
                Utilities.EventLogging.logEvent(null, "No device selected", false, "warning");
                return;
            }
            string deviceMac = m_DeviceTree.SelectedNode.Name;
            string interfaceKey = m_DeviceTree.SelectedNode.Parent.Name;

            //get network
            Network conn = OpenUdpNetwork(interfaceKey, deviceMac);
            if (conn == null) return;

            //disconnect
            try
            {
                conn.UdpTransport.ReleaseRequest(Properties.Settings.Default.DefaultTimeoutMs, Properties.Settings.Default.DefaultRetries);
            }
            catch(Exception ex)
            {
                string trace = "Exception: " + ex.Message;
                Utilities.EventLogging.logEvent(null, trace, false, "warning");
            }
            conn.UdpTransport.Close();
            conn.UdpTransport = null;
        }

        public void loadGSDMLFileToolStripMenuItem_Click(object sender = null, EventArgs e = null)
        {
            //select file
            var dlg = new OpenFileDialog {Title = "Open GDSLML file", Filter = "GDSML|*xml|All|*.*"};
            if (dlg.ShowDialog(this) != System.Windows.Forms.DialogResult.OK) return;
            //load
            var path = dlg.FileName;
            try
            {
                if (LoadGsdmlFile(path) == 0)
                {
                    //add to settings
                    Properties.Settings.Default.GDSMLFilePaths.Add(path);
                }

                //delete GSDMLfile-MenuItems and then add as new list of DropDownMenuItems
                editGSDMLFileToolStripMenuItem.DropDownItems.Clear();
                convertGSDMLFileToolStripMenuItem.DropDownItems.Clear();
                gsdml_MenuItem();

            }
            catch (Exception ex)
            {
                string errorMessage = "Couldn't load GDSML file: " + ex.Message;
                Utilities.EventLogging.logEvent(this,errorMessage, true,"error");
                return;
            }

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HelpMenu.About(this);


        }

        public ISO15745Profile parse_gsd(string path)
        {
            try
            {
                // reads and validates xml against xsd files (gsd specification ISO 15745Profile)
                var xmlDocument = new XmlDocument();
                xmlDocument.Load(path);
                var assembly = Assembly.GetExecutingAssembly();
                var xsdResources = assembly.GetManifestResourceNames().Where(x => x.EndsWith(".xsd")); // gets all resource files with ending ".xsd"

                var settings = new XmlReaderSettings
                    {DtdProcessing = DtdProcessing.Ignore, ValidationType = ValidationType.Schema};

                foreach (var xsdFile in xsdResources) // iterate over resources and adds them to the schema of the document
                {
                    var xsdResourceStream = assembly.GetManifestResourceStream(xsdFile);

                    if (xsdResourceStream == null)
                    {
                        throw new Exception();
                    }

                    var xmlReader = XmlReader.Create(xsdResourceStream, settings);
                    xmlDocument.Schemas.Add(null, xmlReader); // adds schemas to the document
                }

                xmlDocument.Validate(null); // validates xml document with xsds

                //creates gsd object -> easier access than plan xml
                var serializer = new XmlSerializer(typeof(ISO15745Profile));
                ISO15745Profile gsd = null;
                using (var reader = new StreamReader(path))
                {

                    gsd = (ISO15745Profile)serializer.Deserialize(reader);
                    gsd.FilePath = path; //saves filepath in object
                    return gsd;
                }
            }
            catch (Exception e)
            {

                string[] stringSeparators = new string[] {"\n   "};
                var stList = e.ToString().Split(stringSeparators, StringSplitOptions.None);


                Utilities.EventLogging.logEvent(this,
                    "Error while opening gsd file: " + path + "\n\nErrormessage:\n" + stList[0], true, "error");
                //Utilities.EventLogging.logFailedEvent(this, errorMessage, false, "error");

                //Trace.TraceInformation();
                //Trace.TraceInformation("Errormessage: " + stList[0]);
                return null;
            }

        }

      
        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            HelpMenu.ReadMe(this);
        }

        // adding all loaded GSDML files as DropDownMenuItems for editing and converting
        private void gsdml_MenuItem()
        {
            foreach (string row in Properties.Settings.Default.GDSMLFilePaths)
            {
                string rw = Path.GetFileNameWithoutExtension(row);
                ToolStripMenuItem SubMenu1 = new ToolStripMenuItem(rw, null);
                SubMenu1.Click += editgsdMenuItem_Click;
                editGSDMLFileToolStripMenuItem.DropDownItems.Add(SubMenu1);
            }

            foreach (string row in Properties.Settings.Default.GDSMLFilePaths)
            {
                string rw = Path.GetFileNameWithoutExtension(row);
                ToolStripMenuItem SubMenu2 = new ToolStripMenuItem(rw, null);
                SubMenu2.Click += convertgsdMenuItem_Click;
                convertGSDMLFileToolStripMenuItem.DropDownItems.Add(SubMenu2);
            }
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //delete GSDMLfile-MenuItems and then add as new list of DropDownMenuItems
            editGSDMLFileToolStripMenuItem.DropDownItems.Clear();
            convertGSDMLFileToolStripMenuItem.DropDownItems.Clear();
            gsdml_MenuItem();
        }

        // open clicked GSDML file in Windows associcated XML-editor
        private void editgsdMenuItem_Click(object sender, EventArgs e)
        {
            var menuItem = (ToolStripMenuItem)sender;
            var menuText = menuItem.Text;

            for (int i = 0; i < Properties.Settings.Default.GDSMLFilePaths.Count; i++)
            {
                string openedfilename = Path.GetFileNameWithoutExtension(Properties.Settings.Default.GDSMLFilePaths[i]);
                string openedfilepath = Properties.Settings.Default.GDSMLFilePaths[i];

                if (openedfilename == menuText) //verify clicked file
                {
                    try
                    {
                        // opening gsdml file and after closing xml-editor reloading the edited file
                        var editor = Process.Start(Properties.Settings.Default.GDSMLFilePaths[i]);
                        editor.EnableRaisingEvents = true;
                        editor.Exited += (sender2, e2) => {
                            Utilities.EventLogging.logEvent(this, "File \"" + openedfilename + "\" has been edited and now reloading ...", false, "info");
                            if (LoadGsdmlFile(openedfilepath) == 0)
                            {
                                Properties.Settings.Default.GDSMLFilePaths.Add(openedfilepath);
                            }                            
                        };
                    }
                    catch (Exception ex)
                    {
                        Utilities.EventLogging.logEvent(this, ex.Message, true, "error");
                    }
                }
            }
        }

        // convert clicked GSDML file into AMLx
        private void convertgsdMenuItem_Click(object sender, EventArgs e)
        {
            var menuItem = (ToolStripMenuItem)sender;
            var menuText = menuItem.Text;
            Utilities.EventLogging.logEvent(this, "Conversion of " + menuText + ".xml to \".aml\" started.", false, "line");
            for (int i = 0; i < Properties.Settings.Default.GDSMLFilePaths.Count; i++)
            {
                string openedFilename = Path.GetFileNameWithoutExtension(Properties.Settings.Default.GDSMLFilePaths[i]);
                if (openedFilename == menuText) //verify clicked file
                {
                    try
                    {
                        string openedFilepath = Properties.Settings.Default.GDSMLFilePaths[i];
                        //var outputDirName = openedFilepath.Substring(0, openedfilepath.LastIndexOf("\\") + 1);
                        var outputFileName = openedFilepath.Substring(0, openedFilepath.LastIndexOf(".")) + ".amlx";
                        Converter.Convert(openedFilepath, outputFileName, true); // test for gsd2aml converter

                    }
                    catch (Exception ex)
                    {
                        Utilities.EventLogging.logEvent(this, ex.Message, true, "error");
                    }
                }
            }
        }
    }
}
