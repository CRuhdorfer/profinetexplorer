﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using Gsd2Aml.Lib.Logging;

namespace Utilities
{
    public class EventLogging : Gsd2Aml.Lib.Logging.ILoggingService
    {
        private IWin32Window _owner;
        #region staticMethods
        private static string logFilePath;

        public static void logEvent(IWin32Window owner, string errorMessage, Boolean visual, string type)
        {
            DateTime currentTimeAndDate = DateTime.Now;
            string logMessage = currentTimeAndDate + " " + firstToUpper(type) + ": " + errorMessage;
            System.IO.File.AppendAllText(@logFilePath, logMessage + Environment.NewLine);
            //System.IO.File.AppendAllText(@logFilePath, "\n");
            if (visual)
            {
                MessageBox.Show(owner, errorMessage, firstToUpper(type), MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            if ("warning".Equals(type, StringComparison.OrdinalIgnoreCase))
            {
                Trace.TraceWarning(logMessage);
            }
            else if ("information".Equals(type, StringComparison.OrdinalIgnoreCase))
            {
                Trace.TraceInformation(logMessage);
            }
            else if ("error".Equals(type, StringComparison.OrdinalIgnoreCase))
            {
                Trace.TraceError(logMessage);
            }
            else if ("line".Equals(type, StringComparison.OrdinalIgnoreCase))
            {
                Trace.WriteLine(logMessage, null);
            }
            else
            {
                Trace.WriteLine(logMessage, null);
            }
        }

        public static void initLogFile(DateTime startTime)
        {
            logFilePath =
                System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    $@"ProfinetExplorer\Logs\{startTime.ToString("yyyy-MM-dd_HH-mm-ss")}.log");

            System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(logFilePath));
        }

        private static string firstToUpper(string content)
        {
            if (content.Length == 1)
            {
                return content.ToUpper();
            }
            else if (content.Length > 1)
            {
                return content.Substring(0, 1).ToUpper() + content.Substring(1);
            }

            return content;
        }
        #endregion

        public EventLogging(IWin32Window owner)
        {
            _owner = owner;
        }

        public void Log(LogLevel level, string message)
        {
            switch (level)
            {
                case LogLevel.Info:
                    logEvent(_owner, message, false, level.ToString());
                    break;
                case LogLevel.Warning:
                    logEvent(_owner, message, false, level.ToString());
                    break;
                case LogLevel.Error:
                    logEvent(_owner, message, false, level.ToString());
                    break;
                default:
                    logEvent(_owner, message, false, level.ToString());
                    break;
            }
        }
    }

    #region " Trace Listner "
    public class MyTraceListener : TraceListener
    {
        private ProfinetExplorer.MainDialog _mForm;

        public MyTraceListener(ProfinetExplorer.MainDialog form)
            : base("MyListener")
        {
            _mForm = form;
        }

        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message)
        {
            if ((this.Filter != null) && !this.Filter.ShouldTrace(eventCache, source, eventType, id, message, null, null, null)) return;

            ConsoleColor color;
            switch (eventType)
            {
                case TraceEventType.Error:
                    color = ConsoleColor.Red;
                    break;
                case TraceEventType.Warning:
                    color = ConsoleColor.Yellow;
                    break;
                case TraceEventType.Information:
                    color = ConsoleColor.DarkGreen;
                    break;
                default:
                    color = ConsoleColor.Gray;
                    break;
            }

            WriteColor(message + Environment.NewLine, color);
        }

        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string format, params object[] args)
        {
            if ((this.Filter != null) && !this.Filter.ShouldTrace(eventCache, source, eventType, id, format, args, null, null)) return;

            ConsoleColor color;
            switch (eventType)
            {
                case TraceEventType.Error:
                    color = ConsoleColor.Red;
                    break;
                case TraceEventType.Warning:
                    color = ConsoleColor.Yellow;
                    break;
                case TraceEventType.Information:
                    color = ConsoleColor.DarkGreen;
                    break;
                default:
                    color = ConsoleColor.Gray;
                    break;
            }

            WriteColor(string.Format(format, args) + Environment.NewLine, color);
        }

        public override void Write(string message)
        {
            WriteColor(message, ConsoleColor.Gray);
        }
        public override void WriteLine(string message)
        {
            WriteColor(message + Environment.NewLine, ConsoleColor.Gray);
        }

        private void WriteColor(string message, ConsoleColor color)
        {
            if (!_mForm.IsHandleCreated) return;

            _mForm.m_LogText.BeginInvoke((MethodInvoker)delegate { _mForm.m_LogText.AppendText(message); });
        }
    }
    #endregion
}