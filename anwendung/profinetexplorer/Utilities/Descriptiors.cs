﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Utilities
{
    /// <summary>
	/// IntToHexCustomTypeDescriptor
	/// </summary>
    public class IntToHexCustomTypeDescriptor : CustomTypeDescriptor
    {
        private IntToHexTypeConverter _mConverter = new IntToHexTypeConverter();
        public override TypeConverter GetConverter()
        {
            return _mConverter;
        }
    }

    /// <summary>
	/// Custom PropertyDescriptor
	/// </summary>
    class CustomPropertyDescriptor : PropertyDescriptor
    {
        CustomProperty _mProperty;
        public CustomPropertyDescriptor(ref CustomProperty myProperty, Attribute[] attrs) : base(myProperty.Name, attrs)
        {
            _mProperty = myProperty;
        }

        public CustomProperty CustomProperty
        {
            get { return _mProperty; }
        }

        #region PropertyDescriptor specific

        public override bool CanResetValue(object component)
        {
            return true;
        }

        public override Type ComponentType
        {
            get
            {
                return null;
            }
        }

        public override object GetValue(object component)
        {
            return _mProperty.Value;
        }

        public override string Description
        {
            get
            {
                return _mProperty.Description;
            }
        }

        public override string Category
        {
            get
            {
                return _mProperty.Category;
            }
        }

        public override string DisplayName
        {
            get
            {
                return _mProperty.Name;
            }

        }

        public override bool IsReadOnly
        {
            get
            {
                return _mProperty.ReadOnly;
            }
        }

        public override void ResetValue(object component)
        {
            _mProperty.Reset();
        }

        public override bool ShouldSerializeValue(object component)
        {
            return false;
        }

        public override void SetValue(object component, object value)
        {
            _mProperty.Value = value;
        }

        public override Type PropertyType
        {
            get { return _mProperty.Type; }
        }

        public override TypeConverter Converter
        {
            get
            {
                if (_mProperty.Type == typeof(DynamicPropertyGridContainer)) return new ExpandableObjectConverter();
                else if (_mProperty.Options != null) return new DynamicEnumConverter(_mProperty.Options);
                else if (_mProperty.Type == typeof(float)) return new CustomSingleConverter();
                else return base.Converter;
            }
        }

        public DynamicEnum Options
        {
            get
            {
                return _mProperty.Options;
            }
        }

        #endregion


    }

    /// <summary>
	/// IntToHexTypeDescriptionProvider
	/// </summary>
    public class IntToHexTypeDescriptionProvider : TypeDescriptionProvider
    {
        private IntToHexCustomTypeDescriptor _mDescriptor = new IntToHexCustomTypeDescriptor();

        // empty constructor required to build the project
        public IntToHexTypeDescriptionProvider(TypeDescriptionProvider parent) :
            base(parent)
        { }

        public override ICustomTypeDescriptor GetTypeDescriptor(Type objectType, object instance)
        {
            if (objectType == typeof(int) || objectType == typeof(uint) || objectType == typeof(short) || objectType == typeof(ushort) || objectType == typeof(byte) || objectType == typeof(sbyte) || objectType == typeof(Int64) || objectType == typeof(UInt64))
            {
                return _mDescriptor;
            }
            else
            {
                return base.GetTypeDescriptor(objectType, instance);
            }
        }
    }
}
