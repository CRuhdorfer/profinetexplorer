﻿/**************************************************************************
*                           MIT License
* 
* Copyright (C) 2014 Morten Kvistgaard <mk@pch-engineering.dk>
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Diagnostics;

namespace Profinet
{
    public class ConnectionInfoUdp
    {
        public ProfinetUdpTransport Adapter { get; set; }
        public IPEndPoint Source { get; set; }
        public ConnectionInfoUdp(ProfinetUdpTransport adapter, IPEndPoint source)
        {
            Adapter = adapter;
            Source = source;
        }
    }

    public class ProfinetUdpTransport
    {
        private UdpClient _mReceiverConn;
        private UdpClient _mSenderConn;
        private IPAddress _mInterfaceIp = null;
        private int _mPort = 0x8894;
        private Guid _mObjectInstance;
        private System.Net.NetworkInformation.PhysicalAddress _mSourceMac;
        private IPAddress _mDestinationIp;
        private uint _mSequenceNo = 0;
        private Dcp.DeviceIdInfo _mDeviceId;
        private Guid _mActivity;
        private Guid _mAruuid;
        private bool _mIsClosed = true;
        private ushort _mInputCycleTimeMs = 16;
        private ushort _mOutputCycleTimeMs = 16;

        public delegate void OnRpcMessageReceivedHandler(ConnectionInfoUdp sender, Rpc.PacketTypes type, Rpc.Flags1 flags1, Rpc.Flags2 flags2, Rpc.Encodings encoding, UInt16 serialHighLow, Guid objectId, Guid interfaceId, Guid activityId, UInt32 serverBootTime, UInt32 sequenceNo, Rpc.Operations op, UInt16 bodyLength, UInt16 fragmentNo, Stream data);
        public event OnRpcMessageReceivedHandler OnRpcMessageReceived;

        public bool HasConnected { get; private set; }
        public ushort InputCycleTimeMs 
        {
            get { return _mInputCycleTimeMs; }
            set
            {
                //find highest 2-base
                ushort n = 512;
                while (n > 0)
                {
                    if ((value / n) > 0)
                    {
                        _mInputCycleTimeMs = n;
                        return;
                    }
                    n >>= 1;
                }
                throw new ArgumentException("Number not supported");
            }
        }
        public ushort OutputCycleTimeMs
        {
            get { return _mOutputCycleTimeMs; }
            set
            {
                //find highest 2-base
                ushort n = 512;
                while (n > 0)
                {
                    if ((value / n) > 0)
                    {
                        _mOutputCycleTimeMs = n;
                        return;
                    }
                    n >>= 1;
                }
                throw new ArgumentException("Number not supported");
            }
        }

        public ProfinetUdpTransport(string interfaceIp, System.Net.NetworkInformation.PhysicalAddress sourceMac)
        {
            _mSourceMac = sourceMac;
            if (!string.IsNullOrEmpty(interfaceIp)) _mInterfaceIp = IPAddress.Parse(interfaceIp);
            _mObjectInstance = Rpc.GenerateObjectInstanceUuid(1, 0, 0x25, 0x120);
        }

        private void OnDataReceived(IAsyncResult result)
        {
            if (_mIsClosed) return;
            UdpClient s = (UdpClient)result.AsyncState;
            try
            {
                IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
                byte[] bytes = s.EndReceive(result, ref sender);
                if (bytes != null && bytes.Length != 0)
                {
                    if(bytes[0] == 4)   //RPC version
                    {
                        MemoryStream mem = new MemoryStream(bytes, false);

                        //RPC
                        Rpc.PacketTypes type; Rpc.Flags1 flags1; Rpc.Flags2 flags2; Rpc.Encodings encoding; UInt16 serialHighLow; Guid objectId; Guid interfaceId; Guid activityId; UInt32 serverBootTime; UInt32 sequenceNo; Rpc.Operations op; UInt16 bodyLength; UInt16 fragmentNo;
                        Rpc.DecodeHeader(mem, out type, out flags1, out flags2, out encoding, out serialHighLow, out objectId, out interfaceId, out activityId, out serverBootTime, out sequenceNo, out op, out bodyLength, out fragmentNo);
                        if(OnRpcMessageReceived != null) OnRpcMessageReceived(new ConnectionInfoUdp(this, sender), type, flags1, flags2, encoding, serialHighLow, objectId, interfaceId, activityId, serverBootTime, sequenceNo, op, bodyLength, fragmentNo, mem);
                    }
                    else
                        Utilities.EventLogging.logEvent(null, "Something else received on udp port", false, "warning");
                }
                else
                {
                    // Empty frame : port scanner maybe
                }
            }
            catch (Exception ex)
            {
                string trace = "Udp recive error: " + ex.Message;
                Utilities.EventLogging.logEvent(null, trace, false, "warning");
            }
            try
            {
                s.BeginReceive(new AsyncCallback(OnDataReceived), s);
            }
            catch (ObjectDisposedException)
            {
                //close down
            }
            catch (Exception)
            {
                if (_mIsClosed) return;
                else throw;
            }
        }

        private UdpClient CreateServerSocket(int port)
        {
            //create transport
            IPEndPoint local = new IPEndPoint(_mInterfaceIp, port);
            UdpClient conn = new UdpClient();
            conn.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            conn.Client.Bind(local);
            conn.EnableBroadcast = true;
            conn.MulticastLoopback = false;
            return conn;
        }

        public void Open(string destinationIp, Dcp.DeviceIdInfo deviceId)
        {
            //create transport, bind to first free port
            //this is the wrong way to do it. You're supposed to create an outgoing socket and receive 'responses' on that
            //However, the Renesas TPS-1 is sending the response from a diffierent src port, thereby breaking the conversation
            //to fix this we can setup a server socket instead
            _mSenderConn = CreateServerSocket(0);
            _mSenderConn.BeginReceive(new AsyncCallback(OnDataReceived), _mSenderConn);
            _mDeviceId = deviceId;
            _mDestinationIp = IPAddress.Parse(destinationIp);

            //we also need another server socket, to receive stuff
            _mReceiverConn = CreateServerSocket(_mPort);
            _mIsClosed = false;
            _mReceiverConn.BeginReceive(new AsyncCallback(OnDataReceived), _mReceiverConn);
        }

        public IAsyncResult BeginConnectRequest()
        {
            //create new activity id
            _mActivity = Guid.NewGuid();
            _mAruuid = Guid.NewGuid();

            //Create RPC packet
            long bodyLengthPosition;
            long ndrDataHeaderPosition;
            MemoryStream mem = new MemoryStream();
            Rpc.Encodings encoding = Rpc.Encodings.Ascii | Rpc.Encodings.LittleEndian | Rpc.Encodings.IeeeFloat;
            Guid destinationInstance = Rpc.GenerateObjectInstanceUuid(1, 0, _mDeviceId.DeviceId, _mDeviceId.VendorId);
            Rpc.EncodeHeader(mem, Rpc.PacketTypes.Request, Rpc.Flags1.Idempotent, 0, encoding, 0, destinationInstance, Rpc.UuidIoDeviceInterface, _mActivity, 0, _mSequenceNo++, Rpc.Operations.Connect, 0, 0, out bodyLengthPosition);
            Rpc.EncodeNdrDataHeader(mem, encoding, 0, 0, 0, 0, 0, out ndrDataHeaderPosition);

            //Create IO Packet (I have no idea what most of this is)
            ProfinetIo.ArBlockRequest arBlockReq = new ProfinetIo.ArBlockRequest();
            arBlockReq.Type = ProfinetIo.ArTypes.IocarSingle;
            arBlockReq.Uuid = _mAruuid;
            arBlockReq.SessionKey = 1;
            arBlockReq.CmInitiatorMac = _mSourceMac;
            arBlockReq.CmInitiatorObjectUuid = _mObjectInstance;
            arBlockReq.Properties = ProfinetIo.ArProperties.StateActive | ProfinetIo.ArProperties.ParameterizationServerCmInitiator;
            arBlockReq.CmInitiatorActivityTimeoutFactor = 600;
            arBlockReq.CmInitiatorUdprtPort = 0x8892;
            arBlockReq.CmInitiatorStationName = "ProfinetExplorer";
            ProfinetIo.IocrBlockRequest[] iocrBlockReq = new ProfinetIo.IocrBlockRequest[2]{new ProfinetIo.IocrBlockRequest(), new ProfinetIo.IocrBlockRequest()};

            iocrBlockReq[0].Type = ProfinetIo.IocrTypes.Input;
            iocrBlockReq[0].Reference = 1;
            iocrBlockReq[0].Properties = ProfinetIo.IocrProperties.RtClass1;
            iocrBlockReq[0].DataLength = 216;
            iocrBlockReq[0].FrameId = 0xc000;
            iocrBlockReq[0].SendClockFactor = 32;     //clock cycle is 1 ms
            iocrBlockReq[0].ReductionRatio = _mInputCycleTimeMs;
            iocrBlockReq[0].Phase = 1;
            iocrBlockReq[0].Sequence = 0;
            iocrBlockReq[0].FrameSendOffset = 0xFFFFFFFF;
            iocrBlockReq[0].DataHoldFactorA = 3;
            iocrBlockReq[0].DataHoldFactorB = 3;
            iocrBlockReq[0].TagHeader = ProfinetIo.IocrTagHeaders.IoUserPriorityCr;
            iocrBlockReq[0].MulticastMac = new System.Net.NetworkInformation.PhysicalAddress(new byte[] {0,0,0,0,0,0});
            iocrBlockReq[0].ApIs = new ProfinetIo.IocApi[1];
            iocrBlockReq[0].ApIs[0] = new ProfinetIo.IocApi();
            iocrBlockReq[0].ApIs[0].No = 0;
            iocrBlockReq[0].ApIs[0].DataObjects = new ProfinetIo.IocDataObject[5];
            iocrBlockReq[0].ApIs[0].DataObjects[0] = new ProfinetIo.IocDataObject(0, 0x8000,0);
            iocrBlockReq[0].ApIs[0].DataObjects[1] = new ProfinetIo.IocDataObject(0, 0x8001, 1);
            iocrBlockReq[0].ApIs[0].DataObjects[2] = new ProfinetIo.IocDataObject(0, 0x8002, 2);
            iocrBlockReq[0].ApIs[0].DataObjects[3] = new ProfinetIo.IocDataObject(0, 1, 3);
            iocrBlockReq[0].ApIs[0].DataObjects[4] = new ProfinetIo.IocDataObject(1, 1, 5);
            iocrBlockReq[0].ApIs[0].Iocs = new ProfinetIo.IocDataObject[1];
            iocrBlockReq[0].ApIs[0].Iocs[0] = new ProfinetIo.IocDataObject(1,1,4);

            iocrBlockReq[1].Type = ProfinetIo.IocrTypes.Output;
            iocrBlockReq[1].Reference = 2;
            iocrBlockReq[1].Properties = ProfinetIo.IocrProperties.RtClass1;
            iocrBlockReq[1].DataLength = 40;
            iocrBlockReq[1].FrameId = 0xc001;
            iocrBlockReq[1].SendClockFactor = 32;     //clock cycle is 1 ms
            iocrBlockReq[1].ReductionRatio = _mOutputCycleTimeMs;
            iocrBlockReq[1].Phase = 1;
            iocrBlockReq[1].Sequence = 0;
            iocrBlockReq[1].FrameSendOffset = 0xFFFFFFFF;
            iocrBlockReq[1].DataHoldFactorA = 3;
            iocrBlockReq[1].DataHoldFactorB = 3;
            iocrBlockReq[1].TagHeader = ProfinetIo.IocrTagHeaders.IoUserPriorityCr;
            iocrBlockReq[1].MulticastMac = new System.Net.NetworkInformation.PhysicalAddress(new byte[] { 0, 0, 0, 0, 0, 0 });
            iocrBlockReq[1].ApIs = new ProfinetIo.IocApi[1];
            iocrBlockReq[1].ApIs[0] = new ProfinetIo.IocApi();
            iocrBlockReq[1].ApIs[0].No = 0;
            iocrBlockReq[1].ApIs[0].DataObjects = new ProfinetIo.IocDataObject[1];
            iocrBlockReq[1].ApIs[0].DataObjects[0] = new ProfinetIo.IocDataObject(1, 1, 5);
            iocrBlockReq[1].ApIs[0].Iocs = new ProfinetIo.IocDataObject[5];
            iocrBlockReq[1].ApIs[0].Iocs[0] = new ProfinetIo.IocDataObject(0, 0x8000, 0);
            iocrBlockReq[1].ApIs[0].Iocs[1] = new ProfinetIo.IocDataObject(0, 0x8001, 1);
            iocrBlockReq[1].ApIs[0].Iocs[2] = new ProfinetIo.IocDataObject(0, 0x8002, 2);
            iocrBlockReq[1].ApIs[0].Iocs[3] = new ProfinetIo.IocDataObject(0, 0x1, 3);
            iocrBlockReq[1].ApIs[0].Iocs[4] = new ProfinetIo.IocDataObject(1, 0x1, 4);

            ProfinetIo.AlarmCrBlockRequest alarmCrBlockReq = new ProfinetIo.AlarmCrBlockRequest();
            alarmCrBlockReq.Properties = ProfinetIo.AlarmCrProperties.RtaClass1 | ProfinetIo.AlarmCrProperties.UserPriority;
            alarmCrBlockReq.RtaTimeoutFactor = 1;
            alarmCrBlockReq.RtaRetries = 3;
            alarmCrBlockReq.LocalAlarmReference = 1;
            alarmCrBlockReq.MaxAlarmDataLength = 256;
            alarmCrBlockReq.AlarmCrTagHeaderHigh = 0xC000;
            alarmCrBlockReq.AlarmCrTagHeaderLow = 0xA000;

            ProfinetIo.ExpectedSubmoduleBlockRequest[] expectedSubmoduleBlockReq = new ProfinetIo.ExpectedSubmoduleBlockRequest[2]{new ProfinetIo.ExpectedSubmoduleBlockRequest(), new ProfinetIo.ExpectedSubmoduleBlockRequest()};
            expectedSubmoduleBlockReq[0].ApIs = new ProfinetIo.SubmoduleApi[1];
            expectedSubmoduleBlockReq[0].ApIs[0] = new ProfinetIo.SubmoduleApi();
            expectedSubmoduleBlockReq[0].ApIs[0].No = 0;
            expectedSubmoduleBlockReq[0].ApIs[0].SlotNumber = 0;
            expectedSubmoduleBlockReq[0].ApIs[0].ModuleIdentNumber = 1;
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules = new ProfinetIo.Submodule[4];
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[0] = new ProfinetIo.Submodule();
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[0].SubslotNumber = 0x8000;
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[0].SubmoduleIdentNumber = 0xA;
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[0].SubmoduleProperties = ProfinetIo.SubmoduleProperties.TypeNoIo | ProfinetIo.SubmoduleProperties.SharedInputIoControllerOnly | ProfinetIo.SubmoduleProperties.ReduceInputSubmoduleDataLengthExpected | ProfinetIo.SubmoduleProperties.ReduceOutputSubmoduleDataLengthExpected | ProfinetIo.SubmoduleProperties.DiscardIoxsExpected;
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[0].DataDescription = new ProfinetIo.DataDescription[1];
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[0].DataDescription[0] = new ProfinetIo.DataDescription(ProfinetIo.DataDescription.Types.Input, 0, 1, 1);
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[1] = new ProfinetIo.Submodule();
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[1].SubslotNumber = 0x8001;
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[1].SubmoduleIdentNumber = 0xB;
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[1].SubmoduleProperties = ProfinetIo.SubmoduleProperties.TypeNoIo | ProfinetIo.SubmoduleProperties.SharedInputIoControllerOnly | ProfinetIo.SubmoduleProperties.ReduceInputSubmoduleDataLengthExpected | ProfinetIo.SubmoduleProperties.ReduceOutputSubmoduleDataLengthExpected | ProfinetIo.SubmoduleProperties.DiscardIoxsExpected;
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[1].DataDescription = new ProfinetIo.DataDescription[1];
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[1].DataDescription[0] = new ProfinetIo.DataDescription(ProfinetIo.DataDescription.Types.Input, 0, 1, 1);
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[2] = new ProfinetIo.Submodule();
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[2].SubslotNumber = 0x8002;
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[2].SubmoduleIdentNumber = 0xC;
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[2].SubmoduleProperties = ProfinetIo.SubmoduleProperties.TypeNoIo | ProfinetIo.SubmoduleProperties.SharedInputIoControllerOnly | ProfinetIo.SubmoduleProperties.ReduceInputSubmoduleDataLengthExpected | ProfinetIo.SubmoduleProperties.ReduceOutputSubmoduleDataLengthExpected | ProfinetIo.SubmoduleProperties.DiscardIoxsExpected;
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[2].DataDescription = new ProfinetIo.DataDescription[1];
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[2].DataDescription[0] = new ProfinetIo.DataDescription(ProfinetIo.DataDescription.Types.Input, 0, 1, 1);
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[3] = new ProfinetIo.Submodule();
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[3].SubslotNumber = 1;
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[3].SubmoduleIdentNumber = 1;
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[3].SubmoduleProperties = ProfinetIo.SubmoduleProperties.TypeNoIo | ProfinetIo.SubmoduleProperties.SharedInputIoControllerOnly | ProfinetIo.SubmoduleProperties.ReduceInputSubmoduleDataLengthExpected | ProfinetIo.SubmoduleProperties.ReduceOutputSubmoduleDataLengthExpected | ProfinetIo.SubmoduleProperties.DiscardIoxsExpected;
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[3].DataDescription = new ProfinetIo.DataDescription[1];
            expectedSubmoduleBlockReq[0].ApIs[0].SubModules[3].DataDescription[0] = new ProfinetIo.DataDescription(ProfinetIo.DataDescription.Types.Input, 0, 1, 1);

            expectedSubmoduleBlockReq[1].ApIs = new ProfinetIo.SubmoduleApi[1];
            expectedSubmoduleBlockReq[1].ApIs[0] = new ProfinetIo.SubmoduleApi();
            expectedSubmoduleBlockReq[1].ApIs[0].No = 0;
            expectedSubmoduleBlockReq[1].ApIs[0].SlotNumber = 1;
            expectedSubmoduleBlockReq[1].ApIs[0].ModuleIdentNumber = 2;
            expectedSubmoduleBlockReq[1].ApIs[0].SubModules = new ProfinetIo.Submodule[1];
            expectedSubmoduleBlockReq[1].ApIs[0].SubModules[0] = new ProfinetIo.Submodule();
            expectedSubmoduleBlockReq[1].ApIs[0].SubModules[0].SubslotNumber = 1;
            expectedSubmoduleBlockReq[1].ApIs[0].SubModules[0].SubmoduleIdentNumber = 2;
            expectedSubmoduleBlockReq[1].ApIs[0].SubModules[0].SubmoduleProperties = ProfinetIo.SubmoduleProperties.TypeInputOutput | ProfinetIo.SubmoduleProperties.SharedInputIoControllerOnly | ProfinetIo.SubmoduleProperties.ReduceInputSubmoduleDataLengthExpected | ProfinetIo.SubmoduleProperties.ReduceOutputSubmoduleDataLengthExpected | ProfinetIo.SubmoduleProperties.DiscardIoxsExpected;
            expectedSubmoduleBlockReq[1].ApIs[0].SubModules[0].DataDescription = new ProfinetIo.DataDescription[2];
            expectedSubmoduleBlockReq[1].ApIs[0].SubModules[0].DataDescription[0] = new ProfinetIo.DataDescription(ProfinetIo.DataDescription.Types.Input, 210, 1, 1);
            expectedSubmoduleBlockReq[1].ApIs[0].SubModules[0].DataDescription[1] = new ProfinetIo.DataDescription(ProfinetIo.DataDescription.Types.Output, 4, 1, 1);
            ProfinetIo.EncodeConnectRequest(mem, arBlockReq, iocrBlockReq, alarmCrBlockReq, expectedSubmoduleBlockReq, null, null, null, null, null, null, null);

            //re-encode rpc lengths
            Rpc.ReEncodeHeaderLength(mem, encoding, bodyLengthPosition);
            Rpc.ReEncodeNdrDataHeaderLength(mem, encoding, ndrDataHeaderPosition, true);

            //send
            Utilities.EventLogging.logEvent(null, "Sending Connect message", false, "information");
            HasConnected = true;
            return new ProfinetAsyncRpcResult(this, mem);
        }

        public IAsyncResult BeginWriteRequest(IEnumerable<KeyValuePair<ProfinetIo.IodWriteReqHeader, byte[]>> values)
        {
            //Create RPC packet
            long bodyLengthPosition;
            long ndrDataHeaderPosition;
            MemoryStream mem = new MemoryStream();
            Rpc.Encodings encoding = Rpc.Encodings.Ascii | Rpc.Encodings.LittleEndian | Rpc.Encodings.IeeeFloat;
            Guid destinationInstance = Rpc.GenerateObjectInstanceUuid(1, 0, _mDeviceId.DeviceId, _mDeviceId.VendorId);
            Rpc.EncodeHeader(mem, Rpc.PacketTypes.Request, Rpc.Flags1.Idempotent, 0, encoding, 0, destinationInstance, Rpc.UuidIoDeviceInterface, _mActivity, 0, _mSequenceNo++, Rpc.Operations.Write, 0, 0, out bodyLengthPosition);
            Rpc.EncodeNdrDataHeader(mem, encoding, 0, 0, 0, 0, 0, out ndrDataHeaderPosition);

            //Create IO Packet
            ProfinetIo.EncodeWriteMultipleRequest(mem, _mAruuid, values);

            //re-encode rpc lengths
            Rpc.ReEncodeHeaderLength(mem, encoding, bodyLengthPosition);
            Rpc.ReEncodeNdrDataHeaderLength(mem, encoding, ndrDataHeaderPosition, true);

            //send
            Utilities.EventLogging.logEvent(null, "Sending Write message", false, "information");
            return new ProfinetAsyncRpcResult(this, mem);
        }

        public IAsyncResult BeginControlRequest(ProfinetIo.BlockTypes blockType, ProfinetIo.ControlCommands control)
        {
            //Create RPC packet
            long bodyLengthPosition;
            long ndrDataHeaderPosition;
            MemoryStream mem = new MemoryStream();
            Rpc.Encodings encoding = Rpc.Encodings.Ascii | Rpc.Encodings.LittleEndian | Rpc.Encodings.IeeeFloat;
            Guid destinationInstance = Rpc.GenerateObjectInstanceUuid(1, 0, _mDeviceId.DeviceId, _mDeviceId.VendorId);
            Rpc.EncodeHeader(mem, Rpc.PacketTypes.Request, Rpc.Flags1.Idempotent, 0, encoding, 0, destinationInstance, Rpc.UuidIoDeviceInterface, _mActivity, 0, _mSequenceNo++, Rpc.Operations.Control, 0, 0, out bodyLengthPosition);
            Rpc.EncodeNdrDataHeader(mem, encoding, 0, 0, 0, 0, 0, out ndrDataHeaderPosition);

            //Create IO Packet
            ProfinetIo.ControlBlockConnect ctrl = new ProfinetIo.ControlBlockConnect();
            ctrl.BlockType = blockType;
            ctrl.Aruuid = _mAruuid;
            ctrl.SessionKey = 1;
            ctrl.ControlCommand = control;
            ProfinetIo.EncodeControlRequest(mem, ctrl);

            //re-encode rpc lengths
            Rpc.ReEncodeHeaderLength(mem, encoding, bodyLengthPosition);
            Rpc.ReEncodeNdrDataHeaderLength(mem, encoding, ndrDataHeaderPosition, true);

            //send
            Utilities.EventLogging.logEvent(null, "Sending Control message", false, "information");
            return new ProfinetAsyncRpcResult(this, mem);
        }

        public IAsyncResult BeginReleaseRequest()
        {
            //Create RPC packet
            long bodyLengthPosition;
            long ndrDataHeaderPosition;
            MemoryStream mem = new MemoryStream();
            Rpc.Encodings encoding = Rpc.Encodings.Ascii | Rpc.Encodings.LittleEndian | Rpc.Encodings.IeeeFloat;
            Guid destinationInstance = Rpc.GenerateObjectInstanceUuid(1, 0, _mDeviceId.DeviceId, _mDeviceId.VendorId);
            Rpc.EncodeHeader(mem, Rpc.PacketTypes.Request, Rpc.Flags1.Idempotent, 0, encoding, 0, destinationInstance, Rpc.UuidIoDeviceInterface, _mActivity, 0, _mSequenceNo++, Rpc.Operations.Release, 0, 0, out bodyLengthPosition);
            Rpc.EncodeNdrDataHeader(mem, encoding, 0, 0, 0, 0, 0, out ndrDataHeaderPosition);

            //Create IO Packet
            ProfinetIo.EncodeReleaseRequest(mem, _mAruuid, 1);

            //re-encode rpc lengths
            Rpc.ReEncodeHeaderLength(mem, encoding, bodyLengthPosition);
            Rpc.ReEncodeNdrDataHeaderLength(mem, encoding, ndrDataHeaderPosition, true);

            //send
            Utilities.EventLogging.logEvent(null, "Sending Release message", false, "information");
            return new ProfinetAsyncRpcResult(this, mem);
        }

        public void ControlResponse(Guid activity, uint sequenceNo, ProfinetIo.BlockTypes blockType, ProfinetIo.ControlCommands control)
        {
            //Create RPC packet
            long bodyLengthPosition;
            long ndrDataHeaderPosition;
            MemoryStream mem = new MemoryStream();
            Rpc.Encodings encoding = Rpc.Encodings.Ascii | Rpc.Encodings.LittleEndian | Rpc.Encodings.IeeeFloat;
            Guid destinationInstance = Rpc.GenerateObjectInstanceUuid(1, 0, _mDeviceId.DeviceId, _mDeviceId.VendorId);
            Rpc.EncodeHeader(mem, Rpc.PacketTypes.Response, Rpc.Flags1.LastFragment | Rpc.Flags1.NoFragmentAckRequested, 0, encoding, 0, destinationInstance, Rpc.UuidIoDeviceInterface, activity, 0, sequenceNo, Rpc.Operations.Control, 0, 0, out bodyLengthPosition);
            Rpc.EncodeNdrDataHeader(mem, encoding, 0, 0, 0, 0, 0, out ndrDataHeaderPosition);

            //Create IO Packet
            ProfinetIo.ControlBlockConnect ctrl = new ProfinetIo.ControlBlockConnect();
            ctrl.BlockType = blockType;
            ctrl.Aruuid = _mAruuid;
            ctrl.SessionKey = 1;
            ctrl.ControlCommand = control;
            ProfinetIo.EncodeControlResponse(mem, ctrl);

            //re-encode rpc lengths
            Rpc.ReEncodeHeaderLength(mem, encoding, bodyLengthPosition);
            Rpc.ReEncodeNdrDataHeaderLength(mem, encoding, ndrDataHeaderPosition, false);

            //send
            Utilities.EventLogging.logEvent(null, "Sending Connect response", false, "information");
            SendResponse(mem);
        }

        public void EndTransmit(IAsyncResult result, int timeoutMs)
        {
            ProfinetAsyncRpcResult r = (ProfinetAsyncRpcResult)result;

            if (result.AsyncWaitHandle.WaitOne(timeoutMs))
            {
                r.Dispose();
                return;
            }
            else
            {
                r.Dispose();
                throw new TimeoutException("No response received");
            }
        }

        public void ConnectRequest(int timeoutMs, int retries)
        {
            for (int r = 0; r < retries; r++)
            {
                IAsyncResult asyncResult = BeginConnectRequest();
                try
                {
                    EndTransmit(asyncResult, timeoutMs);
                    return;
                }
                catch (TimeoutException)
                {
                    //continue
                }
            }
            throw new TimeoutException("No response received");
        }

        public void ReleaseRequest(int timeoutMs, int retries)
        {
            for (int r = 0; r < retries; r++)
            {
                IAsyncResult asyncResult = BeginReleaseRequest();
                try
                {
                    EndTransmit(asyncResult, timeoutMs);
                    return;
                }
                catch (TimeoutException)
                {
                    //continue
                }
            }
            throw new TimeoutException("No response received");
        }

        public void WriteRequest(IEnumerable<KeyValuePair<ProfinetIo.IodWriteReqHeader, byte[]>> values, int timeoutMs, int retries)
        {
            for (int r = 0; r < retries; r++)
            {
                IAsyncResult asyncResult = BeginWriteRequest(values);
                try
                {
                    EndTransmit(asyncResult, timeoutMs);
                    return;
                }
                catch (TimeoutException)
                {
                    //continue
                }
            }
            throw new TimeoutException("No response received");
        }

        public enum Controls
        {
            ParameterEnd,
        }

        public void ControlRequest(Controls c, int timeoutMs, int retries)
        {
            ProfinetIo.BlockTypes blockType;
            ProfinetIo.ControlCommands control;

            switch (c)
            {
                case Controls.ParameterEnd:
                    blockType = ProfinetIo.BlockTypes.IodBlockReqControlBlockConnectPrmEnd;
                    control = ProfinetIo.ControlCommands.PrmEnd;
                    break;
                default:
                    throw new NotImplementedException();
            }

            for (int r = 0; r < retries; r++)
            {
                IAsyncResult asyncResult = BeginControlRequest(blockType, control);
                try
                {
                    EndTransmit(asyncResult, timeoutMs);
                    return;
                }
                catch (TimeoutException)
                {
                    //continue
                }
            }
            throw new TimeoutException("No response received");
        }

        public class ProfinetAsyncRpcResult : IAsyncResult, IDisposable
        {
            private System.Threading.ManualResetEvent _mWait = new System.Threading.ManualResetEvent(false);
            private ProfinetUdpTransport _mConn = null;

            public object AsyncState { get; set; }
            public System.Threading.WaitHandle AsyncWaitHandle { get { return _mWait; } }
            public bool CompletedSynchronously { get; private set; }    //always false
            public bool IsCompleted { get; private set; }

            public Stream Result { get; private set; }
            public Rpc.Encodings Encoding { get; private set; }

            public ProfinetAsyncRpcResult(ProfinetUdpTransport conn, MemoryStream message)
            {
                _mConn = conn;
                conn.OnRpcMessageReceived += new OnRpcMessageReceivedHandler(conn_OnRPCMessageReceived);
                conn.SendRequest(message);
            }

            private void conn_OnRPCMessageReceived(ConnectionInfoUdp sender, Rpc.PacketTypes type, Rpc.Flags1 flags1, Rpc.Flags2 flags2, Rpc.Encodings encoding, ushort serialHighLow, Guid objectId, Guid interfaceId, Guid activityId, uint serverBootTime, uint sequenceNo, Rpc.Operations op, ushort bodyLength, ushort fragmentNo, Stream data)
            {
                //TODO: compare something to validate correct response

                //set result
                Result = data;
                Encoding = encoding;

                //mark as finished
                IsCompleted = true;
                _mWait.Set();
                Dispose();
            }

            public void Dispose()
            {
                if (_mConn != null)
                {
                    _mConn.OnRpcMessageReceived -= conn_OnRPCMessageReceived;
                    _mConn = null;
                }
            }
        }

        public void Close()
        {
            _mIsClosed = true;
            if (_mSenderConn != null)
            {
                _mSenderConn.Close();
                _mSenderConn = null;
            }
            if (_mReceiverConn != null)
            {
                _mReceiverConn.Close();
                _mReceiverConn = null;
            }
        }

        private void SendRequest(MemoryStream stream)
        {
            IPEndPoint ep = new IPEndPoint(_mDestinationIp, _mPort);
            byte[] buffer = stream.GetBuffer();
            int tx = _mSenderConn.Send(buffer, (int)stream.Position, ep);
        }

        private void SendResponse(MemoryStream stream)
        {
            IPEndPoint ep = new IPEndPoint(_mDestinationIp, _mPort);
            byte[] buffer = stream.GetBuffer();
            int tx = _mReceiverConn.Send(buffer, (int)stream.Position, ep);
        }
    }
}
