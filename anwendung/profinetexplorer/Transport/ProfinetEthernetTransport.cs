﻿/**************************************************************************
*                           MIT License
* 
* Copyright (C) 2014 Morten Kvistgaard <mk@pch-engineering.dk>
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using SharpPcap;
using System.Net.NetworkInformation;
using System.Net;
using System.IO;
using System.Diagnostics;

namespace Profinet
{
    public class ConnectionInfoEthernet
    {
        public ProfinetEthernetTransport Adapter;
        public PhysicalAddress Destination;
        public PhysicalAddress Source;
        public ConnectionInfoEthernet(ProfinetEthernetTransport adapter, PhysicalAddress destination, PhysicalAddress source)
        {
            Adapter = adapter;
            Destination = destination;
            Source = source;
        }
    }

    public class ProfinetEthernetTransport : IDisposable
    {
        private ICaptureDevice _mAdapter;
        private UInt16 _mLastXid = 0;
        private bool _mIsOpen = false;

        public delegate void OnDcpMessageHandler(ConnectionInfoEthernet sender, Dcp.ServiceIds serviceId, uint xid, ushort responseDelayFactor, Dictionary<Dcp.BlockOptions, object> blocks);
        public event OnDcpMessageHandler OnDcpMessage;
        public delegate void OnAcyclicMessageHandler(ConnectionInfoEthernet sender, UInt16 alarmDestinationEndpoint, UInt16 alarmSourceEndpoint, Rt.PduTypes pduType, Rt.AddFlags addFlags, UInt16 sendSeqNum, UInt16 ackSeqNum, UInt16 varPartLen, Stream data);
        public event OnAcyclicMessageHandler OnAcyclicMessage;
        public delegate void OnCyclicMessageHandler(ConnectionInfoEthernet sender, UInt16 cycleCounter, Rt.DataStatus dataStatus, Rt.TransferStatus transferStatus, Stream data, int dataLength);
        public event OnCyclicMessageHandler OnCyclicMessage;

        public bool IsOpen { get { return _mIsOpen; } }
        public ICaptureDevice Adapter { get { return _mAdapter; } }

        public ProfinetEthernetTransport(ICaptureDevice adapter)
        {
            _mAdapter = adapter;
            _mAdapter.OnPacketArrival += new PacketArrivalEventHandler(m_adapter_OnPacketArrival);   
        }

        /// <summary>
        /// Will return pcap version. Use this to validate installed pcap library
        /// </summary>
        public static string PcapVersion
        {
            get
            {
                try
                {
                    return SharpPcap.Pcap.Version;
                }
                catch { }
                return "";
            }
        }

        public void Open()
        {
            if (_mIsOpen) return;
            if (_mAdapter is SharpPcap.WinPcap.WinPcapDevice)
                ((SharpPcap.WinPcap.WinPcapDevice)_mAdapter).Open(SharpPcap.WinPcap.OpenFlags.MaxResponsiveness | SharpPcap.WinPcap.OpenFlags.NoCaptureLocal, -1);
            else
                _mAdapter.Open(DeviceMode.Normal);
            _mAdapter.Filter = "ether proto 0x8892 or vlan 0";
            _mAdapter.StartCapture();
            _mIsOpen = true;
            System.Threading.Thread.Sleep(50);  //let the pcap start up
        }

        public void Close()
        {
            if (!_mIsOpen) return;
            try
            {
                _mAdapter.StopCapture();
            }
            catch
            {
            }
            _mIsOpen = false;
        }

        public void Dispose()
        {
            if (_mAdapter != null)
            {
                Close();
                _mAdapter.Close();
                _mAdapter = null;
            }
        }

        public static Dcp.IpInfo GetPcapIp(SharpPcap.ICaptureDevice pcapDevice)
        {
            foreach (System.Net.NetworkInformation.NetworkInterface nic in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
            {
                PhysicalAddress mac = null;
                try
                {
                    mac = nic.GetPhysicalAddress();
                }
                catch (Exception)
                {
                    //interface have no mac address
                }

                if (mac != null && mac.Equals(pcapDevice.MacAddress))
                {
                    IPInterfaceProperties ipp = nic.GetIPProperties();
                    foreach (var entry in ipp.UnicastAddresses)
                    {
                        if (entry.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            byte[] gw = new byte[] {0,0,0,0 };
                            if(ipp.GatewayAddresses.Count > 0) gw = ipp.GatewayAddresses[0].Address.GetAddressBytes();
                            return new Dcp.IpInfo(Dcp.BlockInfo.IpSet, entry.Address.GetAddressBytes(), entry.IPv4Mask.GetAddressBytes(), gw);
                        }
                    }
                }
            }
            return null;
        }

        public static PhysicalAddress GetDeviceMac(string interfaceIp)
        {
            IPAddress searchIp = IPAddress.Parse(interfaceIp);
            foreach (System.Net.NetworkInformation.NetworkInterface nic in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
            {
                PhysicalAddress mac = null;
                try
                {
                    mac = nic.GetPhysicalAddress();
                }
                catch (Exception)
                {
                    //interface have no mac address
                    continue;
                }
                foreach (var entry in nic.GetIPProperties().UnicastAddresses)
                {
                    if (searchIp.Equals(entry.Address))
                    {
                        return mac;
                    }
                }
            }
            return null;
        }

        public static SharpPcap.ICaptureDevice GetPcapDevice(string localIp)
        {
            IPAddress searchIp = IPAddress.Parse(localIp);
            PhysicalAddress searchMac = null;
            Dictionary<PhysicalAddress, SharpPcap.ICaptureDevice> networks = new Dictionary<PhysicalAddress, SharpPcap.ICaptureDevice>();

            //search all networks
            foreach (System.Net.NetworkInformation.NetworkInterface nic in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
            {
                PhysicalAddress mac = null;
                try
                {
                    mac = nic.GetPhysicalAddress();
                }
                catch (Exception)
                {
                    //interface have no mac address
                    continue;
                }
                foreach (var entry in nic.GetIPProperties().UnicastAddresses)
                {
                    if (searchIp.Equals(entry.Address))
                    {
                        searchMac = mac;
                        break;
                    }
                }
                if (searchMac != null) break;
            }

            //validate
            if (searchMac == null) return null;

            //search all pcap networks
            foreach (SharpPcap.ICaptureDevice dev in SharpPcap.CaptureDeviceList.Instance)
            {
                try
                {
                    dev.Open();
                    networks.Add(dev.MacAddress, dev);
                    dev.Close();
                }
                catch { }
            }

            //find link
            if (networks.ContainsKey(searchMac)) return networks[searchMac];
            else return null;
        }

        public static System.Net.IPAddress GetNetworkAddress(System.Net.IPAddress address, System.Net.IPAddress subnetMask)
        {
            byte[] ipAdressBytes = address.GetAddressBytes();
            byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

            if (ipAdressBytes.Length != subnetMaskBytes.Length)
                throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

            byte[] broadcastAddress = new byte[ipAdressBytes.Length];
            for (int i = 0; i < broadcastAddress.Length; i++)
                broadcastAddress[i] = (byte)(ipAdressBytes[i] & (subnetMaskBytes[i]));
            return new System.Net.IPAddress(broadcastAddress);
        }

        public static string GetLocalIpAddress(string ipMatch)
        {
            System.Net.IPAddress target = System.Net.IPAddress.Parse(ipMatch);
            foreach (System.Net.NetworkInformation.NetworkInterface nic in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
            {
                foreach (var entry in nic.GetIPProperties().UnicastAddresses)
                {
                    if (entry.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        if (GetNetworkAddress(entry.Address, entry.IPv4Mask).Equals(GetNetworkAddress(target, entry.IPv4Mask)))
                            return entry.Address.ToString();
                    }
                }
            }
            return "";
        }

        private void m_adapter_OnProfinetArrival(ConnectionInfoEthernet sender, Stream stream)
        {
            Rt.FrameIds frameId;

            //Real Time
            Rt.DecodeFrameId(stream, out frameId);
            if (frameId == Rt.FrameIds.DcpIdentifyResPdu || frameId == Rt.FrameIds.DcpIdentifyReqPdu || frameId == Rt.FrameIds.DcpGetSetPdu || frameId == Rt.FrameIds.DcpHelloReqPdu)
            {
                Dcp.ServiceIds serviceId;
                uint xid;
                ushort responseDelayFactor;
                ushort dcpDataLength;
                Dcp.DecodeHeader(stream, out serviceId, out xid, out responseDelayFactor, out dcpDataLength);
                Dictionary<Dcp.BlockOptions, object> blocks;
                Dcp.DecodeAllBlocks(stream, dcpDataLength, out blocks);
                if (OnDcpMessage != null) OnDcpMessage(sender, serviceId, xid, responseDelayFactor, blocks);
            }
            else if (frameId == Rt.FrameIds.PtcpDelayReqPdu)
            {
                //ignore this for now
            }
            else if (frameId >= Rt.FrameIds.RtcStart && frameId <= Rt.FrameIds.RtcEnd)
            {
                long dataPos = stream.Position;
                stream.Position = stream.Length - 4;
                UInt16 cycleCounter;
                Rt.DataStatus dataStatus;
                Rt.TransferStatus transferStatus;
                Rt.DecodeRtcStatus(stream, out cycleCounter, out dataStatus, out transferStatus);
                stream.Position = dataPos;
                if (OnCyclicMessage != null) OnCyclicMessage(sender, cycleCounter, dataStatus, transferStatus, stream, (int)(stream.Length - dataPos - 4));
            }
            else if (frameId == Rt.FrameIds.AlarmLow || frameId == Rt.FrameIds.AlarmHigh)
            {
                UInt16 alarmDestinationEndpoint;
                UInt16 alarmSourceEndpoint;
                Rt.PduTypes pduType;
                Rt.AddFlags addFlags;
                UInt16 sendSeqNum;
                UInt16 ackSeqNum;
                UInt16 varPartLen;
                Rt.DecodeRtaHeader(stream, out alarmDestinationEndpoint, out alarmSourceEndpoint, out pduType, out addFlags, out sendSeqNum, out ackSeqNum, out varPartLen);
                if(OnAcyclicMessage != null) OnAcyclicMessage(sender, alarmDestinationEndpoint, alarmSourceEndpoint, pduType, addFlags, sendSeqNum, ackSeqNum, varPartLen, stream);
            }
            else
            {
                Utilities.EventLogging.logEvent(null, "Unclassified RT message", false, "warning");
            }
        }

        private void m_adapter_OnPacketArrival(object sender, CaptureEventArgs e)
        {
            if(e.Packet.LinkLayerType != PacketDotNet.LinkLayers.Ethernet) return;
            PacketDotNet.Utils.ByteArraySegment bas = new PacketDotNet.Utils.ByteArraySegment(e.Packet.Data);
            PacketDotNet.EthernetPacket ethP = new PacketDotNet.EthernetPacket(bas);
            if (ethP.Type != (PacketDotNet.EthernetType)0x8892 && ethP.Type != PacketDotNet.EthernetType.VLanTaggedFrame) return;
            if (ethP.PayloadPacket != null && ethP.PayloadPacket is PacketDotNet.Ieee8021QPacket)
            {
                if (((PacketDotNet.Ieee8021QPacket)ethP.PayloadPacket).Type != (PacketDotNet.EthernetType)0x8892) return;
                if (((PacketDotNet.Ieee8021QPacket)ethP.PayloadPacket).PayloadData == null)
                {
                    Utilities.EventLogging.logEvent(null, "Empty vlan package", false, "warning");
                    return;
                }
                m_adapter_OnProfinetArrival(new ConnectionInfoEthernet(this, ethP.DestinationHardwareAddress, ethP.SourceHardwareAddress), new MemoryStream(((PacketDotNet.Ieee8021QPacket)ethP.PayloadPacket).PayloadData, false));
            }
            else
            {
                if (ethP.PayloadData == null)
                {
                    Utilities.EventLogging.logEvent(null, "Empty ethernet package", false, "warning");
                    return;
                }
                m_adapter_OnProfinetArrival(new ConnectionInfoEthernet(this, ethP.DestinationHardwareAddress, ethP.SourceHardwareAddress), new MemoryStream(ethP.PayloadData, false));
            }
        }

        private void Send(MemoryStream stream)
        {
            byte[] buffer = stream.GetBuffer();
            _mAdapter.SendPacket(buffer, (int)stream.Position);
        }

        public void SendIdentifyBroadcast()
        {
            Utilities.EventLogging.logEvent(null, "Sending identify broadcast", false, "line");

            MemoryStream mem = new MemoryStream();

            //ethernet
            PhysicalAddress ethernetDestinationHwAddress = PhysicalAddress.Parse(Rt.MulticastMacAddIdentifyAddress);
            Ethernet.Encode(mem, ethernetDestinationHwAddress, _mAdapter.MacAddress, Ethernet.Type.VLanTaggedFrame);

            //VLAN
            Vlan.Encode(mem, Vlan.Priorities.Priority0, Vlan.Type.Pn);

            //Profinet Real Time
            Rt.EncodeFrameId(mem, Rt.FrameIds.DcpIdentifyReqPdu);

            //Profinet DCP
            Dcp.EncodeIdentifyRequest(mem, ++_mLastXid);

            //Send
            Send(mem);
        }

        public void SendIdentifyResponse(PhysicalAddress destination, uint xid, Dictionary<Dcp.BlockOptions, object> blocks)
        {
            Utilities.EventLogging.logEvent(null, "Sending identify broadcast", false, "line");

            MemoryStream mem = new MemoryStream();

            //ethernet
            Ethernet.Encode(mem, destination, _mAdapter.MacAddress, Ethernet.Type.VLanTaggedFrame);

            //VLAN
            Vlan.Encode(mem, Vlan.Priorities.Priority0, Vlan.Type.Pn);

            //Profinet Real Time
            Rt.EncodeFrameId(mem, Rt.FrameIds.DcpIdentifyResPdu);

            //Profinet DCP
            Dcp.EncodeIdentifyResponse(mem, xid, blocks);

            //Send
            Send(mem);
        }

        public void SendCyclicData(PhysicalAddress destination, UInt16 frameId, UInt16 cycleCounter, byte[] userData)
        {
            Utilities.EventLogging.logEvent(null, "Sending cyclic data", false, "line");

            MemoryStream mem = new MemoryStream();

            //ethernet
            Ethernet.Encode(mem, destination, _mAdapter.MacAddress, (Ethernet.Type)0x8892);

            //Profinet Real Time
            Rt.EncodeFrameId(mem, (Rt.FrameIds)frameId);

            //user data
            if (userData == null) userData = new byte[40];
            if (userData.Length < 40) Array.Resize<byte>(ref userData, 40);
            mem.Write(userData, 0, userData.Length);
            
            //RT footer
            Rt.EncodeRtcStatus(mem, cycleCounter, Rt.DataStatus.DataItemValid |
                                                    Rt.DataStatus.StatePrimary |
                                                    Rt.DataStatus.ProviderStateRun |
                                                    Rt.DataStatus.StationProblemIndicatorNormal,
                                                    Rt.TransferStatus.Ok);

            //Send
            Send(mem);
        }

        public class ProfinetAsyncDcpResult : IAsyncResult, IDisposable
        {
            private System.Threading.ManualResetEvent _mWait = new System.Threading.ManualResetEvent(false);
            private ushort _mXid;
            private ProfinetEthernetTransport _mConn = null;

            public object AsyncState { get; set; }
            public System.Threading.WaitHandle AsyncWaitHandle { get { return _mWait;} }
            public bool CompletedSynchronously { get; private set; }    //always false
            public bool IsCompleted { get; private set; }

            public Dictionary<Dcp.BlockOptions, object> Result { get; private set; }

            public ProfinetAsyncDcpResult(ProfinetEthernetTransport conn, MemoryStream message, UInt16 xid)
            {
                _mConn = conn;
                conn.OnDcpMessage += new OnDcpMessageHandler(conn_OnDcpMessage);
                _mXid = xid;
                conn.Send(message);
            }

            private void conn_OnDcpMessage(ConnectionInfoEthernet sender, Dcp.ServiceIds serviceId, uint xid, ushort responseDelayFactor, Dictionary<Dcp.BlockOptions, object> blocks)
            {
                if (xid == _mXid)
                {
                    Result = blocks;
                    IsCompleted = true;
                    _mWait.Set();
                }
            }

            public void Dispose()
            {
                if (_mConn != null)
                {
                    _mConn.OnDcpMessage -= conn_OnDcpMessage;
                    _mConn = null;
                }
            }
        }

        public IAsyncResult BeginGetRequest(PhysicalAddress destination, Dcp.BlockOptions option)
        {
            Utilities.EventLogging.logEvent(null, "Sending Get " + option.ToString() + " request", false, "line");

            MemoryStream mem = new MemoryStream();

            //ethernet
            Ethernet.Encode(mem, destination, _mAdapter.MacAddress, Ethernet.Type.VLanTaggedFrame);

            //VLAN
            Vlan.Encode(mem, Vlan.Priorities.Priority0, Vlan.Type.Pn);

            //Profinet Real Time
            Rt.EncodeFrameId(mem, Rt.FrameIds.DcpGetSetPdu);

            //Profinet DCP
            UInt16 xid = ++_mLastXid;
            Dcp.EncodeGetRequest(mem, xid, option);
            //start Async
            return new ProfinetAsyncDcpResult(this, mem, xid);
        }

        public void SendGetResponse(PhysicalAddress destination, uint xid, Dcp.BlockOptions option, object data)
        {
            Utilities.EventLogging.logEvent(null, "Sending Get " + option.ToString() + " response", false, "line");

            MemoryStream mem = new MemoryStream();

            //ethernet
            Ethernet.Encode(mem, destination, _mAdapter.MacAddress, Ethernet.Type.VLanTaggedFrame);

            //VLAN
            Vlan.Encode(mem, Vlan.Priorities.Priority0, Vlan.Type.Pn);

            //Profinet Real Time
            Rt.EncodeFrameId(mem, Rt.FrameIds.DcpGetSetPdu);

            //Profinet DCP
            Dcp.EncodeGetResponse(mem, xid, option, data);
            
            //send
            Send(mem);
        }

        public IAsyncResult BeginSetRequest(PhysicalAddress destination, Dcp.BlockOptions option, Dcp.BlockQualifiers qualifiers, byte[] data)
        {
            Utilities.EventLogging.logEvent(null, "Sending Set " + option.ToString() + " request", false, "line");


            MemoryStream mem = new MemoryStream();

            //ethernet
            Ethernet.Encode(mem, destination, _mAdapter.MacAddress, Ethernet.Type.VLanTaggedFrame);

            //VLAN
            Vlan.Encode(mem, Vlan.Priorities.Priority0, Vlan.Type.Pn);

            //Profinet Real Time
            Rt.EncodeFrameId(mem, Rt.FrameIds.DcpGetSetPdu);

            //Profinet DCP
            UInt16 xid = ++_mLastXid;
            Dcp.EncodeSetRequest(mem, xid, option, qualifiers, data);

            //start Async
            return new ProfinetAsyncDcpResult(this, mem, xid);
        }

        public void SendSetResponse(PhysicalAddress destination, uint xid, Dcp.BlockOptions option, Dcp.BlockErrors status)
        {
            Utilities.EventLogging.logEvent(null, "Sending Set " + option.ToString() + " response", false, "line");

            MemoryStream mem = new MemoryStream();

            //ethernet
            Ethernet.Encode(mem, destination, _mAdapter.MacAddress, Ethernet.Type.VLanTaggedFrame);

            //VLAN
            Vlan.Encode(mem, Vlan.Priorities.Priority0, Vlan.Type.Pn);

            //Profinet Real Time
            Rt.EncodeFrameId(mem, Rt.FrameIds.DcpGetSetPdu);

            //Profinet DCP
            Dcp.EncodeSetResponse(mem, xid, option, status);

            //Send
            Send(mem);
        }

        public Dcp.BlockErrors EndSetRequest(IAsyncResult result, int timeoutMs)
        {
            ProfinetAsyncDcpResult r = (ProfinetAsyncDcpResult)result;

            if (result.AsyncWaitHandle.WaitOne(timeoutMs))
            {
                Dcp.BlockErrors ret = ((Dcp.ResponseStatus)r.Result[Dcp.BlockOptions.ControlResponse]).Error;
                r.Dispose();
                return ret;
            }
            else
            {
                r.Dispose();
                throw new TimeoutException("No response received");
            }
        }

        public Dictionary<Dcp.BlockOptions, object> EndGetRequest(IAsyncResult result, int timeoutMs)
        {
            ProfinetAsyncDcpResult r = (ProfinetAsyncDcpResult)result;

            if (result.AsyncWaitHandle.WaitOne(timeoutMs))
            {
                Dictionary<Dcp.BlockOptions, object> ret = r.Result;
                r.Dispose();
                return ret;
            }
            else
            {
                r.Dispose();
                throw new TimeoutException("No response received");
            }
        }

        public IAsyncResult BeginSetSignalRequest(PhysicalAddress destination)
        {
            return BeginSetRequest(destination, Dcp.BlockOptions.ControlSignal, Dcp.BlockQualifiers.Temporary, BitConverter.GetBytes((ushort)0x100));      //SignalValue - Flash once
        }

        public IAsyncResult BeginSetResetRequest(PhysicalAddress destination)
        {
            return BeginSetRequest(destination, Dcp.BlockOptions.ControlFactoryReset, Dcp.BlockQualifiers.Permanent, null);
        }

        public IAsyncResult BeginSetNameRequest(PhysicalAddress destination, string name)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(name);
            return BeginSetRequest(destination, Dcp.BlockOptions.DevicePropertiesNameOfStation, Dcp.BlockQualifiers.Permanent, bytes);
        }

        public IAsyncResult BeginSetIpRequest(PhysicalAddress destination, IPAddress ip, IPAddress subnetMask, IPAddress gateway)
        {
            byte[] bytes = new byte[12];
            Array.Copy(ip.GetAddressBytes(), 0, bytes, 0, 4);
            Array.Copy(subnetMask.GetAddressBytes(), 0, bytes, 4, 4);
            Array.Copy(gateway.GetAddressBytes(), 0, bytes, 8, 4);
            return BeginSetRequest(destination, Dcp.BlockOptions.IpIpParameter, Dcp.BlockQualifiers.Permanent, bytes);
        }

        public IAsyncResult BeginSetIpFullRequest(PhysicalAddress destination, IPAddress ip, IPAddress subnetMask, IPAddress gateway, IPAddress[] dns)
        {
            byte[] bytes = new byte[28];
            Array.Copy(ip.GetAddressBytes(), 0, bytes, 0, 4);
            Array.Copy(subnetMask.GetAddressBytes(), 0, bytes, 4, 4);
            Array.Copy(gateway.GetAddressBytes(), 0, bytes, 8, 4);
            if (dns == null || dns.Length != 4) throw new ArgumentException("dns array length must be 4");
            for (int i = 0; i < 4; i++)
                Array.Copy(dns[i].GetAddressBytes(), 0, bytes, 12+i*4, 4);
            return BeginSetRequest(destination, Dcp.BlockOptions.IpFullIpSuite, Dcp.BlockQualifiers.Permanent, bytes);
        }

        public Dcp.BlockErrors SendSetRequest(PhysicalAddress destination, int timeoutMs, int retries, Dcp.BlockOptions option, Dcp.BlockQualifiers qualifiers, byte[] data)
        {
            for (int r = 0; r < retries; r++)
            {
                IAsyncResult asyncResult = BeginSetRequest(destination, option, qualifiers, data);
                try
                {
                    return EndSetRequest(asyncResult, timeoutMs);
                }
                catch (TimeoutException)
                {
                    //continue
                }
            }
            throw new TimeoutException("No response received");
        }

        public Dcp.BlockErrors SendSetRequest(PhysicalAddress destination, int timeoutMs, int retries, Dcp.BlockOptions option, byte[] data)
        {
            return SendSetRequest(destination, timeoutMs, retries, option, Dcp.BlockQualifiers.Permanent, data);
        }

        public Dictionary<Dcp.BlockOptions, object> SendGetRequest(PhysicalAddress destination, int timeoutMs, int retries, Dcp.BlockOptions option)
        {
            for (int r = 0; r < retries; r++)
            {
                IAsyncResult asyncResult = BeginGetRequest(destination, option);
                try
                {
                    return EndGetRequest(asyncResult, timeoutMs);
                }
                catch (TimeoutException)
                {
                    //continue
                }
            }
            throw new TimeoutException("No response received");
        }

        public Dcp.BlockErrors SendSetSignalRequest(PhysicalAddress destination, int timeoutMs, int retries)
        {
            for (int r = 0; r < retries; r++)
            {
                IAsyncResult asyncResult = BeginSetSignalRequest(destination);
                try
                {
                    return EndSetRequest(asyncResult, timeoutMs);
                }
                catch (TimeoutException)
                {
                    //continue
                }
            }
            throw new TimeoutException("No response received");
        }

        public Dcp.BlockErrors SendSetResetRequest(PhysicalAddress destination, int timeoutMs, int retries)
        {
            for (int r = 0; r < retries; r++)
            {
                IAsyncResult asyncResult = BeginSetResetRequest(destination);
                try
                {
                    return EndSetRequest(asyncResult, timeoutMs);
                }
                catch (TimeoutException)
                {
                    //continue
                }
            }
            throw new TimeoutException("No response received");
        }

        public Dcp.BlockErrors SendSetNameRequest(PhysicalAddress destination, int timeoutMs, int retries, string name)
        {
            for (int r = 0; r < retries; r++)
            {
                IAsyncResult asyncResult = BeginSetNameRequest(destination, name);
                try
                {
                    return EndSetRequest(asyncResult, timeoutMs);
                }
                catch (TimeoutException)
                {
                    //continue
                }
            }
            throw new TimeoutException("No response received");
        }

        public Dcp.BlockErrors SendSetIpFullRequest(PhysicalAddress destination, int timeoutMs, int retries, IPAddress ip, IPAddress subnetMask, IPAddress gateway, IPAddress[] dns)
        {
            for (int r = 0; r < retries; r++)
            {
                IAsyncResult asyncResult = BeginSetIpFullRequest(destination, ip, subnetMask, gateway, dns);
                try
                {
                    return EndSetRequest(asyncResult, timeoutMs);
                }
                catch (TimeoutException)
                {
                    //continue
                }
            }
            throw new TimeoutException("No response received");
        }

        public Dcp.BlockErrors SendSetIpRequest(PhysicalAddress destination, int timeoutMs, int retries, IPAddress ip, IPAddress subnetMask, IPAddress gateway)
        {
            for (int r = 0; r < retries; r++)
            {
                IAsyncResult asyncResult = BeginSetIpRequest(destination, ip, subnetMask, gateway);
                try
                {
                    return EndSetRequest(asyncResult, timeoutMs);
                }
                catch (TimeoutException)
                {
                    //continue
                }
            }
            throw new TimeoutException("No response received");
        }
    }
}
