﻿using System;
using System.Windows.Forms;

namespace ProfinetExplorer.MainComponent.Menu
{
    public class HelpMenu
    {
        public static void ReadMe(MainDialog mainDialog)
        {
            string readmePath =
                System.IO.Path.Combine(System.IO.Path.GetDirectoryName(typeof(MainDialog).Assembly.Location),
                    "user_handbook.pdf");
            try
            {
                System.Diagnostics.Process.Start(readmePath);
            }
            catch
            {
                MessageBox.Show(mainDialog,
                    "The help file could not be found on your system, please visit https://gitlab.com/profinetexplorer/profinetexplorer/ for further informations.",
                    "Help", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public static void About(MainDialog mainDialog)
        {
            MessageBox.Show(mainDialog, "ProfinetExplorer\nVersion " + mainDialog.GetType().Assembly.GetName().Version +
                                        "\nBy Morten Kvistgaard - Copyright 2015\n" +
                                        "Ali Dinc, Max Dettmann, Marius Hauser, Lorenz Krause, Bastian Krug, Michael Schweiker, Nicole Wagner - Copyright 2019" +
                                        "\nReference: https://gitlab.com/profinetexplorer/profinetexplorer" +
                                        "\nReference: https://sourceforge.net/projects/profinetexplorer/" +
                                        "\nReference: http://www.famfamfam.com/" +
                                        "\nReference: https://github.com/chmorgan/sharppcap"
                , "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}