﻿namespace ProfinetExplorer
{
    partial class MainDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Networks");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainDialog));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadGSDMLFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editGSDMLFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertGSDMLFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNetworkInterfaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeNetworkInterfaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchForDevicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.lEDFlashToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.connectIOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disconnectIOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addManufactorDCPEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.m_AddInterfaceButton = new System.Windows.Forms.ToolStripButton();
            this.m_RemoveNetworkInterfaceButton = new System.Windows.Forms.ToolStripButton();
            this.m_SearchToolButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.m_LEDFlashButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.m_ConnectIOButton = new System.Windows.Forms.ToolStripButton();
            this.m_DisconnectIOButton = new System.Windows.Forms.ToolStripButton();
            this.m_SplitContainerButtom = new System.Windows.Forms.SplitContainer();
            this.m_SplitContainerLeft = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.m_DeviceTree = new System.Windows.Forms.TreeView();
            this.m_ImageList = new System.Windows.Forms.ImageList(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.m_AddressSpaceTree = new System.Windows.Forms.TreeView();
            this.label3 = new System.Windows.Forms.Label();
            this.m_SplitContainerRight = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.m_AcyclicDataList = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label5 = new System.Windows.Forms.Label();
            this.m_CyclicDataList = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.m_DataGrid = new System.Windows.Forms.PropertyGrid();
            this.label1 = new System.Windows.Forms.Label();
            this.m_LogText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.m_delayedStart = new System.Windows.Forms.Timer(this.components);
            helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_SplitContainerButtom)).BeginInit();
            this.m_SplitContainerButtom.Panel1.SuspendLayout();
            this.m_SplitContainerButtom.Panel2.SuspendLayout();
            this.m_SplitContainerButtom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_SplitContainerLeft)).BeginInit();
            this.m_SplitContainerLeft.Panel1.SuspendLayout();
            this.m_SplitContainerLeft.Panel2.SuspendLayout();
            this.m_SplitContainerLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_SplitContainerRight)).BeginInit();
            this.m_SplitContainerRight.Panel1.SuspendLayout();
            this.m_SplitContainerRight.Panel2.SuspendLayout();
            this.m_SplitContainerRight.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // helpToolStripMenuItem1
            // 
            helpToolStripMenuItem1.Image = global::ProfinetExplorer.Properties.Resources.information;
            helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            helpToolStripMenuItem1.Size = new System.Drawing.Size(133, 26);
            helpToolStripMenuItem1.Text = "Manual";
            helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1012, 28);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadGSDMLFileToolStripMenuItem,
            this.editGSDMLFileToolStripMenuItem,
            this.convertGSDMLFileToolStripMenuItem,
            this.toolStripMenuItem3,
            this.quitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // loadGSDMLFileToolStripMenuItem
            // 
            this.loadGSDMLFileToolStripMenuItem.Image = global::ProfinetExplorer.Properties.Resources.folder_page_white;
            this.loadGSDMLFileToolStripMenuItem.Name = "loadGSDMLFileToolStripMenuItem";
            this.loadGSDMLFileToolStripMenuItem.Size = new System.Drawing.Size(272, 26);
            this.loadGSDMLFileToolStripMenuItem.Text = "Load GSDML file";
            this.loadGSDMLFileToolStripMenuItem.Click += new System.EventHandler(this.loadGSDMLFileToolStripMenuItem_Click);
            // 
            // editGSDMLFileToolStripMenuItem
            // 
            this.editGSDMLFileToolStripMenuItem.Image = global::ProfinetExplorer.Properties.Resources.edit;
            this.editGSDMLFileToolStripMenuItem.Name = "editGSDMLFileToolStripMenuItem";
            this.editGSDMLFileToolStripMenuItem.Size = new System.Drawing.Size(272, 26);
            this.editGSDMLFileToolStripMenuItem.Text = "Edit GSDML file";
            // 
            // convertGSDMLFileToolStripMenuItem
            // 
            this.convertGSDMLFileToolStripMenuItem.Image = global::ProfinetExplorer.Properties.Resources.convert;
            this.convertGSDMLFileToolStripMenuItem.Name = "convertGSDMLFileToolStripMenuItem";
            this.convertGSDMLFileToolStripMenuItem.Size = new System.Drawing.Size(272, 26);
            this.convertGSDMLFileToolStripMenuItem.Text = "Convert GSDML file to AMLx";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(269, 6);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Image = global::ProfinetExplorer.Properties.Resources.cross;
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(272, 26);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNetworkInterfaceToolStripMenuItem,
            this.removeNetworkInterfaceToolStripMenuItem,
            this.searchForDevicesToolStripMenuItem,
            this.toolStripMenuItem1,
            this.lEDFlashToolStripMenuItem,
            this.toolStripMenuItem2,
            this.connectIOToolStripMenuItem,
            this.disconnectIOToolStripMenuItem});
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(83, 24);
            this.searchToolStripMenuItem.Text = "Functions";
            // 
            // addNetworkInterfaceToolStripMenuItem
            // 
            this.addNetworkInterfaceToolStripMenuItem.Image = global::ProfinetExplorer.Properties.Resources.add;
            this.addNetworkInterfaceToolStripMenuItem.Name = "addNetworkInterfaceToolStripMenuItem";
            this.addNetworkInterfaceToolStripMenuItem.Size = new System.Drawing.Size(260, 26);
            this.addNetworkInterfaceToolStripMenuItem.Text = "Add Network Interface";
            this.addNetworkInterfaceToolStripMenuItem.Click += new System.EventHandler(this.addNetworkInterfaceToolStripMenuItem_Click);
            // 
            // removeNetworkInterfaceToolStripMenuItem
            // 
            this.removeNetworkInterfaceToolStripMenuItem.Image = global::ProfinetExplorer.Properties.Resources.cross;
            this.removeNetworkInterfaceToolStripMenuItem.Name = "removeNetworkInterfaceToolStripMenuItem";
            this.removeNetworkInterfaceToolStripMenuItem.Size = new System.Drawing.Size(260, 26);
            this.removeNetworkInterfaceToolStripMenuItem.Text = "Remove Network Interface";
            this.removeNetworkInterfaceToolStripMenuItem.Click += new System.EventHandler(this.removeNetworkInterfaceToolStripMenuItem_Click);
            // 
            // searchForDevicesToolStripMenuItem
            // 
            this.searchForDevicesToolStripMenuItem.Image = global::ProfinetExplorer.Properties.Resources.magnifier;
            this.searchForDevicesToolStripMenuItem.Name = "searchForDevicesToolStripMenuItem";
            this.searchForDevicesToolStripMenuItem.Size = new System.Drawing.Size(260, 26);
            this.searchForDevicesToolStripMenuItem.Text = "Search For Devices";
            this.searchForDevicesToolStripMenuItem.Click += new System.EventHandler(this.searchForDevicesToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(257, 6);
            // 
            // lEDFlashToolStripMenuItem
            // 
            this.lEDFlashToolStripMenuItem.Image = global::ProfinetExplorer.Properties.Resources.lightbulb;
            this.lEDFlashToolStripMenuItem.Name = "lEDFlashToolStripMenuItem";
            this.lEDFlashToolStripMenuItem.Size = new System.Drawing.Size(260, 26);
            this.lEDFlashToolStripMenuItem.Text = "LED Flash";
            this.lEDFlashToolStripMenuItem.Click += new System.EventHandler(this.lEDFlashToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(257, 6);
            // 
            // connectIOToolStripMenuItem
            // 
            this.connectIOToolStripMenuItem.Image = global::ProfinetExplorer.Properties.Resources.control_play;
            this.connectIOToolStripMenuItem.Name = "connectIOToolStripMenuItem";
            this.connectIOToolStripMenuItem.Size = new System.Drawing.Size(260, 26);
            this.connectIOToolStripMenuItem.Text = "Connect IO";
            this.connectIOToolStripMenuItem.Click += new System.EventHandler(this.connectIOToolStripMenuItem_Click);
            // 
            // disconnectIOToolStripMenuItem
            // 
            this.disconnectIOToolStripMenuItem.Image = global::ProfinetExplorer.Properties.Resources.control_stop;
            this.disconnectIOToolStripMenuItem.Name = "disconnectIOToolStripMenuItem";
            this.disconnectIOToolStripMenuItem.Size = new System.Drawing.Size(260, 26);
            this.disconnectIOToolStripMenuItem.Text = "Disconnect IO";
            this.disconnectIOToolStripMenuItem.Click += new System.EventHandler(this.disconnectIOToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.addManufactorDCPEntryToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(73, 24);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Image = global::ProfinetExplorer.Properties.Resources.application_form;
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(261, 26);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // addManufactorDCPEntryToolStripMenuItem
            // 
            this.addManufactorDCPEntryToolStripMenuItem.Image = global::ProfinetExplorer.Properties.Resources.plugin;
            this.addManufactorDCPEntryToolStripMenuItem.Name = "addManufactorDCPEntryToolStripMenuItem";
            this.addManufactorDCPEntryToolStripMenuItem.Size = new System.Drawing.Size(261, 26);
            this.addManufactorDCPEntryToolStripMenuItem.Text = "Add Manufactor DCP entry";
            this.addManufactorDCPEntryToolStripMenuItem.Click += new System.EventHandler(this.addManufactorDCPEntryToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            helpToolStripMenuItem1,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Image = global::ProfinetExplorer.Properties.Resources.information;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(133, 26);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_AddInterfaceButton,
            this.m_RemoveNetworkInterfaceButton,
            this.m_SearchToolButton,
            this.toolStripSeparator1,
            this.m_LEDFlashButton,
            this.toolStripSeparator2,
            this.m_ConnectIOButton,
            this.m_DisconnectIOButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 28);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1012, 27);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // m_AddInterfaceButton
            // 
            this.m_AddInterfaceButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_AddInterfaceButton.Image = global::ProfinetExplorer.Properties.Resources.add;
            this.m_AddInterfaceButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_AddInterfaceButton.Name = "m_AddInterfaceButton";
            this.m_AddInterfaceButton.Size = new System.Drawing.Size(24, 24);
            this.m_AddInterfaceButton.Text = "Add Network Interface";
            this.m_AddInterfaceButton.Click += new System.EventHandler(this.m_AddInterfaceButton_Click);
            // 
            // m_RemoveNetworkInterfaceButton
            // 
            this.m_RemoveNetworkInterfaceButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_RemoveNetworkInterfaceButton.Image = global::ProfinetExplorer.Properties.Resources.cross;
            this.m_RemoveNetworkInterfaceButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_RemoveNetworkInterfaceButton.Name = "m_RemoveNetworkInterfaceButton";
            this.m_RemoveNetworkInterfaceButton.Size = new System.Drawing.Size(24, 24);
            this.m_RemoveNetworkInterfaceButton.Text = "Remove Network Interface";
            this.m_RemoveNetworkInterfaceButton.Click += new System.EventHandler(this.m_RemoveNetworkInterfaceButton_Click);
            // 
            // m_SearchToolButton
            // 
            this.m_SearchToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_SearchToolButton.Image = global::ProfinetExplorer.Properties.Resources.magnifier;
            this.m_SearchToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_SearchToolButton.Name = "m_SearchToolButton";
            this.m_SearchToolButton.Size = new System.Drawing.Size(24, 24);
            this.m_SearchToolButton.Text = "Search For Devices";
            this.m_SearchToolButton.Click += new System.EventHandler(this.m_SearchToolButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // m_LEDFlashButton
            // 
            this.m_LEDFlashButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_LEDFlashButton.Image = global::ProfinetExplorer.Properties.Resources.lightbulb;
            this.m_LEDFlashButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_LEDFlashButton.Name = "m_LEDFlashButton";
            this.m_LEDFlashButton.Size = new System.Drawing.Size(24, 24);
            this.m_LEDFlashButton.Text = "LED Flash";
            this.m_LEDFlashButton.Click += new System.EventHandler(this.m_LEDFlashButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // m_ConnectIOButton
            // 
            this.m_ConnectIOButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_ConnectIOButton.Image = global::ProfinetExplorer.Properties.Resources.control_play;
            this.m_ConnectIOButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_ConnectIOButton.Name = "m_ConnectIOButton";
            this.m_ConnectIOButton.Size = new System.Drawing.Size(24, 24);
            this.m_ConnectIOButton.Text = "Connect IO";
            this.m_ConnectIOButton.Click += new System.EventHandler(this.m_ConnectIOButton_Click);
            // 
            // m_DisconnectIOButton
            // 
            this.m_DisconnectIOButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_DisconnectIOButton.Image = global::ProfinetExplorer.Properties.Resources.control_stop;
            this.m_DisconnectIOButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_DisconnectIOButton.Name = "m_DisconnectIOButton";
            this.m_DisconnectIOButton.Size = new System.Drawing.Size(24, 24);
            this.m_DisconnectIOButton.Text = "Disconnect IO";
            this.m_DisconnectIOButton.Click += new System.EventHandler(this.m_DisconnectIOButton_Click);
            // 
            // m_SplitContainerButtom
            // 
            this.m_SplitContainerButtom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_SplitContainerButtom.Location = new System.Drawing.Point(0, 55);
            this.m_SplitContainerButtom.Margin = new System.Windows.Forms.Padding(4);
            this.m_SplitContainerButtom.Name = "m_SplitContainerButtom";
            this.m_SplitContainerButtom.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // m_SplitContainerButtom.Panel1
            // 
            this.m_SplitContainerButtom.Panel1.Controls.Add(this.m_SplitContainerLeft);
            // 
            // m_SplitContainerButtom.Panel2
            // 
            this.m_SplitContainerButtom.Panel2.Controls.Add(this.m_LogText);
            this.m_SplitContainerButtom.Panel2.Controls.Add(this.label4);
            this.m_SplitContainerButtom.Size = new System.Drawing.Size(1012, 687);
            this.m_SplitContainerButtom.SplitterDistance = 506;
            this.m_SplitContainerButtom.SplitterWidth = 5;
            this.m_SplitContainerButtom.TabIndex = 5;
            // 
            // m_SplitContainerLeft
            // 
            this.m_SplitContainerLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_SplitContainerLeft.Location = new System.Drawing.Point(0, 0);
            this.m_SplitContainerLeft.Margin = new System.Windows.Forms.Padding(4);
            this.m_SplitContainerLeft.Name = "m_SplitContainerLeft";
            // 
            // m_SplitContainerLeft.Panel1
            // 
            this.m_SplitContainerLeft.Panel1.Controls.Add(this.splitContainer4);
            // 
            // m_SplitContainerLeft.Panel2
            // 
            this.m_SplitContainerLeft.Panel2.Controls.Add(this.m_SplitContainerRight);
            this.m_SplitContainerLeft.Size = new System.Drawing.Size(1012, 506);
            this.m_SplitContainerLeft.SplitterDistance = 216;
            this.m_SplitContainerLeft.SplitterWidth = 5;
            this.m_SplitContainerLeft.TabIndex = 0;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.m_DeviceTree);
            this.splitContainer4.Panel1.Controls.Add(this.label2);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.m_AddressSpaceTree);
            this.splitContainer4.Panel2.Controls.Add(this.label3);
            this.splitContainer4.Size = new System.Drawing.Size(216, 506);
            this.splitContainer4.SplitterDistance = 225;
            this.splitContainer4.SplitterWidth = 5;
            this.splitContainer4.TabIndex = 0;
            // 
            // m_DeviceTree
            // 
            this.m_DeviceTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_DeviceTree.ImageIndex = 0;
            this.m_DeviceTree.ImageList = this.m_ImageList;
            this.m_DeviceTree.Location = new System.Drawing.Point(0, 17);
            this.m_DeviceTree.Margin = new System.Windows.Forms.Padding(4);
            this.m_DeviceTree.Name = "m_DeviceTree";
            treeNode1.Name = "Node0";
            treeNode1.Text = "Networks";
            this.m_DeviceTree.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.m_DeviceTree.SelectedImageIndex = 0;
            this.m_DeviceTree.ShowNodeToolTips = true;
            this.m_DeviceTree.ShowRootLines = false;
            this.m_DeviceTree.Size = new System.Drawing.Size(216, 208);
            this.m_DeviceTree.TabIndex = 0;
            this.m_DeviceTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.m_DeviceTree_AfterSelect);
            // 
            // m_ImageList
            // 
            this.m_ImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("m_ImageList.ImageStream")));
            this.m_ImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.m_ImageList.Images.SetKeyName(0, "computer.png");
            this.m_ImageList.Images.SetKeyName(1, "chart_line.png");
            this.m_ImageList.Images.SetKeyName(2, "monitor.png");
            this.m_ImageList.Images.SetKeyName(3, "chart_organisation.png");
            this.m_ImageList.Images.SetKeyName(4, "table.png");
            this.m_ImageList.Images.SetKeyName(5, "note.png");
            this.m_ImageList.Images.SetKeyName(6, "chart_curve.png");
            this.m_ImageList.Images.SetKeyName(7, "contrast.png");
            this.m_ImageList.Images.SetKeyName(8, "disconnect.png");
            this.m_ImageList.Images.SetKeyName(9, "bullet_green.png");
            this.m_ImageList.Images.SetKeyName(10, "bricks.png");
            this.m_ImageList.Images.SetKeyName(11, "folder.png");
            this.m_ImageList.Images.SetKeyName(12, "chart_bar.png");
            this.m_ImageList.Images.SetKeyName(13, "bell.png");
            this.m_ImageList.Images.SetKeyName(14, "calendar.png");
            this.m_ImageList.Images.SetKeyName(15, "date.png");
            this.m_ImageList.Images.SetKeyName(16, "error.png");
            this.m_ImageList.Images.SetKeyName(17, "application_form.png");
            this.m_ImageList.Images.SetKeyName(18, "disk_multiple.png");
            this.m_ImageList.Images.SetKeyName(19, "arrow_refresh.png");
            this.m_ImageList.Images.SetKeyName(20, "page.png");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Devices";
            // 
            // m_AddressSpaceTree
            // 
            this.m_AddressSpaceTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_AddressSpaceTree.ImageIndex = 0;
            this.m_AddressSpaceTree.ImageList = this.m_ImageList;
            this.m_AddressSpaceTree.Location = new System.Drawing.Point(0, 17);
            this.m_AddressSpaceTree.Margin = new System.Windows.Forms.Padding(4);
            this.m_AddressSpaceTree.Name = "m_AddressSpaceTree";
            this.m_AddressSpaceTree.SelectedImageIndex = 0;
            this.m_AddressSpaceTree.ShowNodeToolTips = true;
            this.m_AddressSpaceTree.Size = new System.Drawing.Size(216, 259);
            this.m_AddressSpaceTree.TabIndex = 0;
            this.m_AddressSpaceTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.m_AddressSpaceTree_AfterSelect);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Address Space";
            // 
            // m_SplitContainerRight
            // 
            this.m_SplitContainerRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_SplitContainerRight.Location = new System.Drawing.Point(0, 0);
            this.m_SplitContainerRight.Margin = new System.Windows.Forms.Padding(4);
            this.m_SplitContainerRight.Name = "m_SplitContainerRight";
            // 
            // m_SplitContainerRight.Panel1
            // 
            this.m_SplitContainerRight.Panel1.Controls.Add(this.tabControl1);
            // 
            // m_SplitContainerRight.Panel2
            // 
            this.m_SplitContainerRight.Panel2.Controls.Add(this.m_DataGrid);
            this.m_SplitContainerRight.Panel2.Controls.Add(this.label1);
            this.m_SplitContainerRight.Size = new System.Drawing.Size(791, 506);
            this.m_SplitContainerRight.SplitterDistance = 528;
            this.m_SplitContainerRight.SplitterWidth = 5;
            this.m_SplitContainerRight.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(528, 506);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.m_AcyclicDataList);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.m_CyclicDataList);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(520, 477);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Incomming data";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 247);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "Acyclic (Alarms)";
            // 
            // m_AcyclicDataList
            // 
            this.m_AcyclicDataList.AllowDrop = true;
            this.m_AcyclicDataList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_AcyclicDataList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15});
            this.m_AcyclicDataList.FullRowSelect = true;
            this.m_AcyclicDataList.Location = new System.Drawing.Point(4, 267);
            this.m_AcyclicDataList.Margin = new System.Windows.Forms.Padding(4);
            this.m_AcyclicDataList.Name = "m_AcyclicDataList";
            this.m_AcyclicDataList.Size = new System.Drawing.Size(508, 194);
            this.m_AcyclicDataList.TabIndex = 2;
            this.m_AcyclicDataList.UseCompatibleStateImageBehavior = false;
            this.m_AcyclicDataList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Destination";
            this.columnHeader7.Width = 50;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Source";
            this.columnHeader13.Width = 50;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Type";
            this.columnHeader14.Width = 50;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Info";
            this.columnHeader15.Width = 167;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 4);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Cyclic";
            // 
            // m_CyclicDataList
            // 
            this.m_CyclicDataList.AllowDrop = true;
            this.m_CyclicDataList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_CyclicDataList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader8,
            this.columnHeader9});
            this.m_CyclicDataList.FullRowSelect = true;
            this.m_CyclicDataList.Location = new System.Drawing.Point(4, 23);
            this.m_CyclicDataList.Margin = new System.Windows.Forms.Padding(4);
            this.m_CyclicDataList.Name = "m_CyclicDataList";
            this.m_CyclicDataList.Size = new System.Drawing.Size(508, 219);
            this.m_CyclicDataList.TabIndex = 0;
            this.m_CyclicDataList.UseCompatibleStateImageBehavior = false;
            this.m_CyclicDataList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Cycle";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Status";
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Data";
            // 
            // m_DataGrid
            // 
            this.m_DataGrid.CategoryForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.m_DataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_DataGrid.Location = new System.Drawing.Point(0, 17);
            this.m_DataGrid.Margin = new System.Windows.Forms.Padding(4);
            this.m_DataGrid.Name = "m_DataGrid";
            this.m_DataGrid.Size = new System.Drawing.Size(258, 489);
            this.m_DataGrid.TabIndex = 0;
            this.m_DataGrid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.m_DataGrid_PropertyValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Properties";
            // 
            // m_LogText
            // 
            this.m_LogText.BackColor = System.Drawing.Color.White;
            this.m_LogText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_LogText.Location = new System.Drawing.Point(0, 17);
            this.m_LogText.Margin = new System.Windows.Forms.Padding(4);
            this.m_LogText.Multiline = true;
            this.m_LogText.Name = "m_LogText";
            this.m_LogText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.m_LogText.Size = new System.Drawing.Size(1012, 159);
            this.m_LogText.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Log";
            // 
            // m_delayedStart
            // 
            this.m_delayedStart.Enabled = true;
            this.m_delayedStart.Tick += new System.EventHandler(this.m_delayedStart_Tick);
            // 
            // MainDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 742);
            this.Controls.Add(this.m_SplitContainerButtom);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainDialog";
            this.Text = "ProfinetExplorer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainDialog_FormClosing);
            this.Load += new System.EventHandler(this.MainDialog_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.m_SplitContainerButtom.Panel1.ResumeLayout(false);
            this.m_SplitContainerButtom.Panel2.ResumeLayout(false);
            this.m_SplitContainerButtom.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_SplitContainerButtom)).EndInit();
            this.m_SplitContainerButtom.ResumeLayout(false);
            this.m_SplitContainerLeft.Panel1.ResumeLayout(false);
            this.m_SplitContainerLeft.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_SplitContainerLeft)).EndInit();
            this.m_SplitContainerLeft.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel1.PerformLayout();
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.m_SplitContainerRight.Panel1.ResumeLayout(false);
            this.m_SplitContainerRight.Panel2.ResumeLayout(false);
            this.m_SplitContainerRight.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_SplitContainerRight)).EndInit();
            this.m_SplitContainerRight.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton m_SearchToolButton;
        private System.Windows.Forms.SplitContainer m_SplitContainerButtom;
        private System.Windows.Forms.SplitContainer m_SplitContainerLeft;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.TreeView m_DeviceTree;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TreeView m_AddressSpaceTree;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.SplitContainer m_SplitContainerRight;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ListView m_CyclicDataList;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.PropertyGrid m_DataGrid;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox m_LogText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripMenuItem searchForDevicesToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton m_AddInterfaceButton;
        private System.Windows.Forms.ToolStripButton m_RemoveNetworkInterfaceButton;
        private System.Windows.Forms.ToolStripMenuItem addNetworkInterfaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeNetworkInterfaceToolStripMenuItem;
        private System.Windows.Forms.Timer m_delayedStart;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem lEDFlashToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addManufactorDCPEntryToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton m_LEDFlashButton;
        private System.Windows.Forms.ImageList m_ImageList;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton m_ConnectIOButton;
        private System.Windows.Forms.ToolStripButton m_DisconnectIOButton;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem connectIOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disconnectIOToolStripMenuItem;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListView m_AcyclicDataList;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ToolStripMenuItem loadGSDMLFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem editGSDMLFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convertGSDMLFileToolStripMenuItem;
    }
}

