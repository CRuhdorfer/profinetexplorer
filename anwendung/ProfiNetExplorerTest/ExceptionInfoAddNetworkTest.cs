﻿using System;
using System.Runtime.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProfinetExplorer;
using Assert = NUnit.Framework.Assert;

namespace ProfiNetExplorerTest
{
    [TestClass]
    public class ExceptionInfoAddNetworkTest
    {
        [TestMethod]
        public void AddNetwork_DllNotFound()
        {
            var mainDialog = new MainDialog();
            
            Assert.AreEqual("No WinPCAP found, install WinPCAP first", mainDialog.GetWarningText(new DllNotFoundException()));
        }

        [TestMethod]
        public void AddNetwork_ArgumentOutOfRange()
        {
            var mainDialog = new MainDialog();
            
            Assert.AreEqual("You have to select an Interface to add.", mainDialog.GetWarningText(new ArgumentOutOfRangeException()));
        }

        [TestMethod]
        public void AddNetwork_SharpPcap_Pcap()
        {
            //Due to the data encapsulation of the external Dll SharpPcap, it is not possible to instantiate the required exception. Thus it is not possible to check whether the correct message is returned.
            /*
             To test this case, please follow the following steps:
               1.	Uninstall WinPcap
               2.	Start ProfinetExplorer
               3.	Ignore warning
               4.	Try to add a device
               5.	Warning with "No interface found, please connect a wired interface." should be shown
             */
        }

        [TestMethod]
        public void AddNetwork_AnyException()
        {
            var mainDialog = new MainDialog();

            StringAssert.StartsWith(mainDialog.GetWarningText(new Exception()), "An Exception occured, for further informations search for ");
        }
    }
}
