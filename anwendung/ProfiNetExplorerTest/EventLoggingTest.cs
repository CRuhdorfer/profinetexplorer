﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProfinetExplorer;
using Assert = NUnit.Framework.Assert;

namespace ProfiNetExplorerTest
{
    [TestClass]
    public class EventLoggingTest
    {
        private readonly string _folder = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), $@"ProfinetExplorer\");
        private string logFilePath;
        private DateTime startTime;

        [TestInitialize]
        public void Init()
        {
            CleanDirectory(System.IO.Path.GetDirectoryName(_folder));
            startTime = DateTime.Now;
            logFilePath = System.IO.Path.Combine(_folder, $@"Logs\{startTime.ToString("yyyy-MM-dd_HH-mm-ss")}.log");

            Utilities.EventLogging.initLogFile(startTime);
        }

        /**
         * Test-ID: T-001
         */
        [TestMethod]
        public void IsDirectoryCreated()
        {
            Assert.IsTrue(Directory.Exists(System.IO.Path.GetDirectoryName(logFilePath)));
        }

        /**
         * Test-ID: T-002
         */
        [TestMethod]
        public void IsLogFileCreated()
        {
            Utilities.EventLogging.initLogFile(startTime);
            Utilities.EventLogging.logEvent(null, "messege", false, "info");

            Assert.IsTrue(File.Exists(logFilePath));
        }

        /**
         * Test-ID: T-003
         */
        [TestMethod]
        public void LogWarning()
        {
            Utilities.EventLogging.logEvent(null, "Warning message", false, "warning");
            string line;
            using (var fileReader = new StreamReader(new FileStream(logFilePath, FileMode.Open, FileAccess.Read),
                Encoding.UTF8))
            {
                line = fileReader.ReadLine();
                StringAssert.Contains(line, "Warning: Warning message");
            }
        }

        /**
         * Test-ID: T-004
         */
        [TestMethod]
        public void LogInformation()
        {
            Utilities.EventLogging.logEvent(null, "info message", false, "information");
            string line;
            using (var fileReader = new StreamReader(new FileStream(logFilePath, FileMode.Open, FileAccess.Read),
                Encoding.UTF8))
            {
                line = fileReader.ReadLine();
                StringAssert.Contains(line, "Information: info message");
            }
        }

        /**
         * Test-ID: T-005
         */
        [TestMethod]
        public void LogError()
        {
            Utilities.EventLogging.logEvent(null, "Error message blub blub", false, "error");
            string line;
            using (var fileReader = new StreamReader(new FileStream(logFilePath, FileMode.Open, FileAccess.Read),
                Encoding.UTF8))
            {
                line = fileReader.ReadLine();
                StringAssert.Contains(line, "Error: Error message");
            }
        }

        /**
         * Test-ID: T-006
         */
        [TestMethod]
        public void LogLine()
        {
            Utilities.EventLogging.logEvent(null, "Line message", false, "line");
            string line;
            using (var fileReader = new StreamReader(new FileStream(logFilePath, FileMode.Open, FileAccess.Read),
                Encoding.UTF8))
            {
                line = fileReader.ReadLine();
                StringAssert.Contains(line, "Line: Line message");
            }
        }

        /**
         * Test-ID: T-007
         */
        [TestMethod]
        public void LogOther()
        {
            Utilities.EventLogging.logEvent(null, "Other message", false, "");
            string line;
            using (var fileReader = new StreamReader(new FileStream(logFilePath, FileMode.Open, FileAccess.Read),
                Encoding.UTF8))
            {
                line = fileReader.ReadLine();
                StringAssert.Contains(line, "Other message");
            }
        }

        private void CleanDirectory(string directory)
        {
            CleanDirectory(new DirectoryInfo(directory));
        }

        private void CleanDirectory(DirectoryInfo dir)
        {
            if (dir.Exists)
            {
                foreach (DirectoryInfo directoryInfo in dir.GetDirectories())
                {
                    CleanDirectory(directoryInfo);
                }

                foreach (FileInfo fileInfo in dir.GetFiles())
                {
                    if (fileInfo.Exists)
                    {
                        fileInfo.Delete();
                    }
                }

                if (dir.Exists)
                {
                    dir.Delete();
                }
            }
        }
    }
}
